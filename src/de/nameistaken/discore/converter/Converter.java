package de.nameistaken.discore.converter;

import de.nameistaken.discore.util.Result;

public interface Converter<E> {
    /**
     * Methode, die den angegebenen Text zu einer Instanz der Klasse E umwandelt
     *
     * @param text umzuwandelnder String. Es kann davon ausgegangen werden, dass der String weder leer noch null ist, allerdings nicht, dass der String einer gewünschten Form entspricht
     * @return die aus dem text umgewandelte Instanz der Klasse E
     */
    Result<E> convert(String text);
}
