package de.nameistaken.discore.converter;

import de.nameistaken.discore.DisCore;
import de.nameistaken.discore.bot.DiscordBot;
import de.nameistaken.discore.util.Result;
import de.nameistaken.discore.exceptions.DisCoreException;
import de.nameistaken.discore.util.DCLogger;
import de.nameistaken.discore.util.DisCoreUtils;
import de.nameistaken.discore.matcher.ArgumentType;
import de.nameistaken.discore.matcher.Matcher;
import net.dv8tion.jda.api.entities.*;

import java.beans.PropertyEditor;
import java.beans.PropertyEditorManager;
import java.sql.Timestamp;
import java.util.*;

public final class ConverterManager {
    private static final Map<Class<?>, Converter<?>> CLASS_CONVERTER_MAP = new HashMap<>();

    private static final Converter<User> DEFAULT_USER_CONVERTER = (text) -> {
        Result<User> result;
        if (!text.startsWith("@") && !ArgumentType.USER_DISCORD_MENTION.matches(text)) {
            text = "@" + text;
        }

        List<String> content;
        if ((content = Matcher.getContentIfMatches(text, ArgumentType.USER_ID)) != null
                || (content = Matcher.getContentIfMatches(text, ArgumentType.USER_DISCORD_MENTION)) != null) {
            // @userId oder <@userId>
            String id = content.get(0);
            User user = DiscordBot.getInstance().getJda().getUserById(id);
            if (user == null) {
                result = new Result<>();
                result.setErrorMessage(String.format(DisCore.getConfigValue("message.error.converter.user.unknownid"), id));
            } else {
                result = new Result<>(user);
            }
        } else if ((content = Matcher.getContentIfMatches(text, ArgumentType.USER_WITH_DISCRIMINATOR)) != null) {
            // @user#discriminator
            String username = content.get(0);
            String discriminator = content.get(1);
            User user = DiscordBot.getInstance().getJda().getUserByTag(username, discriminator);
            if (user == null) {
                result = new Result<>();
                result.setErrorMessage(String.format(DisCore.getConfigValue("message.error.converter.user.unknownname"), username));
            } else {
                result = new Result<>(user);
            }
        } else {
            // @username
            String username = text.substring(1);
            Set<User> users = new HashSet<>();
            DiscordBot.getInstance().getJda().getGuilds().forEach(
                    guild -> guild.getMembersByEffectiveName(username, true).forEach(
                            member -> users.add(member.getUser())
                    )
            );
            if (users.isEmpty()) {
                result = new Result<>();
                result.setErrorMessage(String.format(DisCore.getConfigValue("message.error.converter.user.unknownname"), username));
            } else if (users.size() > 1) {
                result = new Result<>();
                result.setErrorMessage(String.format(DisCore.getConfigValue("message.error.converter.user.multipleresults"), username, DisCoreUtils.listToString(new ArrayList<>(users))));
            } else {
                result = new Result<>((User) users.toArray()[0]);
            }
        }
        return result;
    };

    private static final Converter<TextChannel> DEFAULT_TEXTCHANNEL_CONVERTER = (text) -> {
        Result<TextChannel> result;
        if (!text.startsWith("#")) {
            text = "#" + text;
        }

        List<String> content;
        if ((content = Matcher.getContentIfMatches(text, ArgumentType.CHANNEL_ID)) != null
                || (content = Matcher.getContentIfMatches(text, ArgumentType.CHANNEL_DISCORD_MENTION)) != null) {
            // #channelId oder <#channelId>
            String id = content.get(0);
            TextChannel textChannel = DiscordBot.getInstance().getJda().getTextChannelById(id);
            if (textChannel == null) {
                result = new Result<>();
                result.setErrorMessage(String.format(DisCore.getConfigValue("message.error.converter.channel.unknownid"), id));
            } else {
                result = new Result<>(textChannel);
            }
        } else {
            // #username
            String channelName = text.substring(1);
            List<TextChannel> channels = DiscordBot.getInstance().getJda().getTextChannelsByName(channelName, true);
            if (channels.isEmpty()) {
                result = new Result<>();
                result.setErrorMessage(String.format(DisCore.getConfigValue("message.error.converter.channel.unknownname"), channelName));
            } else if (channels.size() > 1) {
                result = new Result<>();
                result.setErrorMessage(String.format(DisCore.getConfigValue("message.error.converter.channel.multipleresults"), channelName, DisCoreUtils.listToString(new ArrayList<>(channels))));
            } else {
                result = new Result<>(channels.get(0));
            }
        }
        return result;
    };

    private static final Converter<Role> DEFAULT_ROLE_CONVERTER = (text) -> {
        Result<Role> result;
        if (!text.startsWith("@")) {
            text = "@" + text;
        }

        List<String> content;
        if ((content = Matcher.getContentIfMatches(text, ArgumentType.ROLE_ID)) != null
                || (content = Matcher.getContentIfMatches(text, ArgumentType.ROLE_DISCORD_MENTION)) != null) {
            // @roleId oder <@roleId>
            String id = content.get(0);
            Role role = DiscordBot.getInstance().getJda().getRoleById(id);
            if (role == null) {
                result = new Result<>();
                result.setErrorMessage(String.format(DisCore.getConfigValue("message.error.converter.role.unknownid"), id));
            } else {
                result = new Result<>(role);
            }
        } else {
            // @rolename
            String rolename = text.substring(1);
            List<Role> roles = DiscordBot.getInstance().getJda().getRolesByName(rolename, true);
            if (roles.isEmpty()) {
                result = new Result<>();
                result.setErrorMessage(String.format(DisCore.getConfigValue("message.error.converter.role.unknownname"), rolename));
            } else if (roles.size() > 1) {
                result = new Result<>();
                result.setErrorMessage(String.format(DisCore.getConfigValue("message.error.converter.role.multipleresults"), rolename, DisCoreUtils.listToString(new ArrayList<>(roles))));
            } else {
                result = new Result<>(roles.get(0));
            }
        }
        return result;
    };

    private static final Converter<Timestamp> DEFAULT_TIMESTAMP_CONVERTER = (text) -> {
        Result<Timestamp> result;
        Calendar calendar = Calendar.getInstance();
        boolean changed = false;
        List<String> content;

        if ((content = Matcher.getContentIfMatches(text, ArgumentType.DATE_NOTIME)) != null) {
            //dd.mm.[yy]yy
            calendar.set(Calendar.DATE, Integer.parseInt(content.get(0)));
            calendar.set(Calendar.MONTH, Integer.parseInt(content.get(1)) - 1);
            calendar.set(Calendar.YEAR, 2000 + Integer.parseInt(content.get(2)));
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.MILLISECOND, 0);
            calendar.set(Calendar.SECOND, 0);
            changed = true;
        } else if ((content = Matcher.getContentIfMatches(text, ArgumentType.DATE_EXACT)) != null) {
            //dd.mm.[yy]yy-hh:mm
            calendar.set(Calendar.DATE, Integer.parseInt(content.get(0)));
            calendar.set(Calendar.MONTH, Integer.parseInt(content.get(1)) - 1);
            calendar.set(Calendar.YEAR, 2000 + Integer.parseInt(content.get(2)));
            calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(content.get(3)));
            calendar.set(Calendar.MINUTE, Integer.parseInt(content.get(4)));
            calendar.set(Calendar.MILLISECOND, 0);
            calendar.set(Calendar.SECOND, 0);
            changed = true;
        } else if ((content = Matcher.getContentIfMatches(text, ArgumentType.DATE_INTERVAL)) != null) {
            //<anzahl><d/w/h/m>
            int value = Integer.parseInt(content.get(0));
            String timeType = content.get(1);

            switch (timeType) {
                case "d":
                    calendar.add(Calendar.DATE, value);
                    break;
                case "w":
                    calendar.add(Calendar.DATE, value * 7);
                    break;
                case "h":
                    calendar.add(Calendar.HOUR_OF_DAY, value);
                    break;
                case "m":
                    calendar.add(Calendar.MINUTE, value);
                    break;
            }
            changed = true;
        } else if ((content = Matcher.getContentIfMatches(text, ArgumentType.DATE_TIME)) != null) {
            //hh:mm
            int hours = calendar.get(Calendar.HOUR_OF_DAY);
            int minutes = calendar.get(Calendar.MINUTE);
            int dateHours = Integer.parseInt(content.get(0));
            int dateMinutes = Integer.parseInt(content.get(1));

            if (dateHours < hours || (dateHours == hours && dateMinutes < minutes)) {
                calendar.add(Calendar.DATE, 1);
            }

            calendar.set(Calendar.HOUR_OF_DAY, dateHours);
            calendar.set(Calendar.MINUTE, dateMinutes);
            calendar.set(Calendar.MILLISECOND, 0);
            calendar.set(Calendar.SECOND, 0);
            changed = true;
        }

        if (changed) {
            result = new Result<>(new Timestamp(calendar.getTimeInMillis()));
        } else {
            try {
                result = new Result<>(Timestamp.valueOf(text));
            } catch (IllegalArgumentException e) {
                result = new Result<>();
                result.setErrorMessage("Ungültiges Argument! \"" + text + "\" ist nicht vom Typ Timestamp");
            }
        }

        return result;
    };

    private static final Converter<Emote> DEFAULT_EMOTE_CONVERTER = (text) -> {
        Result<Emote> result;
        List<String> content;

        if ((content = Matcher.getContentIfMatches(text, ArgumentType.EMOTE_DISCORD_MENTION)) != null) {
            Emote emote = DiscordBot.getInstance().getJda().getEmoteById(content.get(1));
            if (emote == null) {
                result = new Result<>();
                result.setErrorMessage("Das angegebene Emote konnte nicht auf den Servern des Bots gefunden werden.");
            } else {
                result = new Result<>(emote);
            }
        } else {
            result = new Result<>();
            result.setErrorMessage("Ungültiges Argument! \"" + text + "\" ist nicht vom Typ Emote");
        }

        return result;
    };

    private static final Converter<de.nameistaken.discore.util.Emote> DEFAULT_NITEMOTE_CONVERTER = (text) -> {
        Result<de.nameistaken.discore.util.Emote> result;

        de.nameistaken.discore.util.Emote emote = de.nameistaken.discore.util.Emote.getReaction(text);
        if (emote == null) {
            text = text.toUpperCase();
            try {
                emote = de.nameistaken.discore.util.Emote.valueOf(text);
                result = new Result<>(emote);
            } catch (IllegalArgumentException e) {
                result = new Result<>();
                result.setErrorMessage("Ungültiges Argument! \"" + text + "\" ist kein Emote");
            }
        } else {
            result = new Result<>(emote);
        }
        return result;
    };

    private static final Converter<Message> DEFAULT_MESSAGE_CONVERTER = (text) -> {
        Result<Message> result;

        List<String> content;

        if ((content = Matcher.getContentIfMatches(text, ArgumentType.MESSAGELINK)) != null) {
            Guild guild = DiscordBot.getInstance().getJda().getGuildById(content.get(0));
            if (guild != null) {
                TextChannel channel = guild.getTextChannelById(content.get(1));
                if (channel != null) {
                    Message message = channel.retrieveMessageById(content.get(2)).complete();
                    if (message != null) {
                        result = new Result<>(message);
                    } else {
                        result = new Result<>();
                        result.setErrorMessage("Nachricht konnte nicht gefunden werden");
                    }
                } else {
                    result = new Result<>();
                    result.setErrorMessage("Die angegebene Nachricht befindet sich in einem unbekannten Channel");
                }
            } else {
                result = new Result<>();
                result.setErrorMessage("Die angegebene Nachricht befindet sich auf einem unbekannten Server");
            }
        } else {
            result = new Result<>();
            result.setErrorMessage("Ungültiges Argument. Bitte gib einen Link zu einer Nachricht an");
        }
        return result;
    };

    static {
        registerConverter(User.class, DEFAULT_USER_CONVERTER);
        registerConverter(TextChannel.class, DEFAULT_TEXTCHANNEL_CONVERTER);
        registerConverter(Role.class, DEFAULT_ROLE_CONVERTER);
        registerConverter(Timestamp.class, DEFAULT_TIMESTAMP_CONVERTER);
        registerConverter(Emote.class, DEFAULT_EMOTE_CONVERTER);
        registerConverter(Message.class, DEFAULT_MESSAGE_CONVERTER);
        registerConverter(de.nameistaken.discore.util.Emote.class, DEFAULT_NITEMOTE_CONVERTER);
        registerConverter(Byte.class, getPrimitiveConverter(Byte.class));
        registerConverter(byte.class, getPrimitiveConverter(Byte.class));
        registerConverter(Short.class, getPrimitiveConverter(Short.class));
        registerConverter(short.class, getPrimitiveConverter(Short.class));
        registerConverter(Integer.class, getPrimitiveConverter(Integer.class));
        registerConverter(int.class, getPrimitiveConverter(Integer.class));
        registerConverter(Long.class, getPrimitiveConverter(Long.class));
        registerConverter(long.class, getPrimitiveConverter(Long.class));
        registerConverter(Float.class, getPrimitiveConverter(Float.class));
        registerConverter(float.class, getPrimitiveConverter(Float.class));
        registerConverter(Double.class, getPrimitiveConverter(Double.class));
        registerConverter(double.class, getPrimitiveConverter(Double.class));
        registerConverter(Boolean.class, getPrimitiveConverter(Boolean.class));
        registerConverter(boolean.class, getPrimitiveConverter(Boolean.class));
        registerConverter(String.class, Result::new);
    }

    @SuppressWarnings("unchecked")
    private static <E> Converter<E> getPrimitiveConverter(Class<E> clazz) {
        return (text) -> {
            Result<E> result;
            PropertyEditor propertyEditor = PropertyEditorManager.findEditor(clazz);
            try {
                propertyEditor.setAsText(text);
                result = new Result<>((E) propertyEditor.getValue());
            } catch (IllegalArgumentException e) {
                result = new Result<>();
                result.setErrorMessage("Ungültiges Argument! \"" + text + "\" ist nicht vom Typ " + clazz.getSimpleName());
            }
            return result;
        };
    }

    /**
     * @param converterClass Klasse für die der Converter registriert werden soll
     * @param converter      der Converter. Wandelt in ein Result um, das entweder das tatsächliche Ergebnis der Umwandlung enthält oder eine entsprechende Fehlermeldung, weshalb die Umwandlung nicht funktioniert hat
     * @param <E>            Beliebige Klasse für die der Converter registriert werden soll
     */
    public static <E> void registerConverter(Class<E> converterClass, Converter<E> converter) {
        DCLogger.log(DCLogger.Type.INFO, ConverterManager.class, "Registering new converter for class " + converterClass.getName());
        CLASS_CONVERTER_MAP.put(converterClass, converter);
    }

    public static <E> Result<E> convertTo(String text, Class<E> type) {
        if (text == null || text.isEmpty()) {
            throw new IllegalArgumentException("Text may not be empty or null");
        }
        Converter<E> converter = getConverter(type);
        if (converter == null) {
            throw new DisCoreException("Unchecked call of convertTo method - no conversion possible");
        } else {
            return converter.convert(text);
        }
    }

    public static boolean isConverterAvailable(Class<?> type) {
        return getConverter(type) != null;
    }

    @SuppressWarnings("unchecked")
    private static <E> Converter<E> getConverter(Class<E> converterClass) {
        return (Converter<E>) CLASS_CONVERTER_MAP.get(converterClass);
    }

    private ConverterManager() {
    }
}
