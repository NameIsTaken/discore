package de.nameistaken.discore.permission;


import net.dv8tion.jda.api.entities.User;

/**
 * Permission Handler zum Überprüfen einzelner Permissions für User. Implementierung für allgemeine Permissionchecks muss im PermisionManager festgelegt werden
 *
 * @author NameIsTaken
 * @version 22.03.2020
 */
public abstract class PermissionHandler {
    /**
     * Default Permission für jeden, gibt immer true bei einzelnem Permission check zurück
     */
    public static final String EVERYONE_PERMISSION = "EVERYONE";

    /**
     * @param user       User der auf die gegebene Permission gecheckt werden soll
     * @param permission Permission, die der User haben soll. Die Form, die der String haben muss gibt dabei die jeweilige Implementierung vor
     * @return true, falls der User die Permission besitzt, sonst false
     */
    protected abstract boolean checkPermission(User user, String permission);

    /**
     * @param user       User der auf die gegebene Permission gecheckt werden soll
     * @param permission Permission, die der User haben soll. Die Form, die der String haben muss gibt dabei die jeweilige Implementierung vor
     * @return true, falls der User die Permission besitzt oder keine permissions angegeben sind, sonst false
     */
    public boolean hasPermission(User user, String permission) {
        if (permission == null || permission.equals(EVERYONE_PERMISSION)) {
            return true;
        } else {
            return checkPermission(user, permission);
        }
    }

    /**
     * @param user        User der auf die gegebenen Permissions gecheckt werden soll
     * @param permissions Permissions, von denen der User mindestens eine haben soll. Die Form, die die Strings haben müssen gibt dabei die jeweilige Implementierung vor
     * @return true, falls der User eine der Permissions besitzt oder keine permissions angegeben sind, sonst false
     */
    public boolean hasPermission(User user, String... permissions) {
        if (permissions == null) {
            return true;
        } else {
            for (String perm : permissions) {
                if (hasPermission(user, perm)) {
                    return true;
                }
            }
            return false;
        }
    }

    /**
     * @param user        User der auf die gegebenen Permissions gecheckt werden soll
     * @param permissions Permissions, von denen der User alle haben soll. Die Form, die die Strings haben müssen gibt dabei die jeweilige Implementierung vor
     * @return true, falls der User alle angegebenen Permissions besitzt oder keine permissions angegeben sind, sonst false
     */
    public boolean hasAllPermissions(User user, String... permissions) {
        if (permissions == null || permissions.length == 0) {
            return true;
        } else {
            for (String perm : permissions) {
                if (!hasPermission(user, perm)) {
                    return false;
                }
            }
            return true;
        }
    }
}
