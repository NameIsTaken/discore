package de.nameistaken.discore.permission;


import net.dv8tion.jda.api.entities.User;

/**
 * Permission Manager, der einen allgemeinen PermissionHandler definiert, auf den für Standardabfragen (Commands etc) für
 * Permissions zugegriffen werden kann
 *
 * @author NameIsTaken
 * @version 21.03.2020
 */
public final class PermissionManager {
    private static PermissionHandler handler;

    public static void setHandler(PermissionHandler handler) {
        PermissionManager.handler = handler;
    }

    /**
     * @param user       User der auf die gegebene Permission gecheckt werden soll
     * @param permission Permission, die der User haben soll. Die Form, die der String haben muss gibt dabei die jeweilige Implementierung vor
     * @return true, falls der User die Permission besitzt oder keine permissions angegeben sind, sonst false
     */
    public static boolean hasPermission(User user, String permission) {
        return handler.hasPermission(user, permission);
    }

    /**
     * @param user        User der auf die gegebenen Permissions gecheckt werden soll
     * @param permissions Permissions, von denen der User mindestens eine haben soll. Die Form, die die Strings haben müssen gibt dabei die jeweilige Implementierung vor
     * @return true, falls der User eine der Permissions besitzt oder keine permissions angegeben sind, sonst false
     */
    public static boolean hasPermission(User user, String... permissions) {
        return handler.hasPermission(user, permissions);
    }

    /**
     * @param user        User der auf die gegebenen Permissions gecheckt werden soll
     * @param permissions Permissions, von denen der User alle haben soll. Die Form, die die Strings haben müssen gibt dabei die jeweilige Implementierung vor
     * @return true, falls der User alle angegebenen Permissions besitzt oder keine permissions angegeben sind, sonst false
     */
    public static boolean hasAllPermissions(User user, String... permissions) {
        return handler.hasAllPermissions(user, permissions);
    }

    private PermissionManager() {
    }
}
