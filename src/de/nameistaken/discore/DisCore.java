package de.nameistaken.discore;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public final class DisCore {

    /* ==================== verschiedene Limits von Discord - Stand 21.03.2020 ==================== */

    /**
     * Die maximale Anzahl an Mitgliedern auf einem Server
     */
    public static final int SERVER_MEMBER_LIMIT = 250000;

    /**
     * Die maximale Anzahl an Channeln (Text, Voice und Kategorien) auf einem Server
     */
    public static final int SERVER_CHANNEL_LIMIT = 500;

    /**
     * Die maximale Anzahl an Rollen auf einem Server
     */
    public static final int SERVER_ROLES_LIMIT = 250;


    /**
     * Die maximale Anzahl an Zeichen, die das Thema eines Channels haben darf
     */
    public static final int CHANNEL_TOPIC_CHARACTER_LIMIT = 1024;

    /**
     * Die maximale Anzahl an Zeichen, die der Name eines Channels (Text, Voice und Kategorien) haben darf
     */
    public static final int CHANNEL_NAME_CHARACTER_LIMIT = 100;


    /**
     * Die maximale Anzahl an Zeichen, die ein Username oder Nickname lang sein darf
     */
    public static final int NAME_CHARACTER_LIMIT = 32;

    /**
     * Die maximale Anzahl an Zeichen, die pro Nachricht verwendet werden können (bei Mentions etc. zählt der raw Text -> <@123456789...>)
     */
    public static final int MESSAGE_CHARACTER_LIMIT = 2000;


    /**
     * Die maximale Anzahl an Zeichen im gesamten Embed summiert
     */
    public static final int MESSAGE_EMBED_TOTAL_CHARACTER_LIMIT = 6000;

    /**
     * Die maximale Anzahl an Zeichen im Titel einer Embed Message
     */
    public static final int MESSAGE_EMBED_TITLE_CHARACTER_LIMIT = 256;

    /**
     * Die maximale Anzahl an Zeichen des Autorennamen einer Embed Message
     */
    public static final int MESSAGE_EMBED_AUTHOR_NAME_CHARACTER_LIMIT = 256;

    /**
     * Die maximale Anzahl an Zeichen im Text einer Embed Message
     */
    public static final int MESSAGE_EMBED_DESCRIPTION_CHARACTER_LIMIT = 2048;

    /**
     * Die maximale Anzahl an Zeichen im Footer einer Embed Message
     */
    public static final int MESSAGE_EMBED_FOOTER_CHARACTER_LIMIT = 1024;

    /**
     * Die maximale Anzahl an Feldern in einer Embed Message
     */
    public static final int MESSAGE_EMBED_FIELD_LIMIT = 25;

    /**
     * Die maximale Anzahl an Zeichen im Namen eines Embed Field
     */
    public static final int MESSAGE_EMBED_FIELD_NAME_CHARACTER_LIMIT = 256;

    /**
     * Die maximale Anzahl an Zeichen im Text eines Embed Field
     */
    public static final int MESSAGE_EMBED_FIELD_VALUE_CHARACTER_LIMIT = 1024;


    private static final String configFileName = "discoreconfig.properties";

    private static Properties disCoreConfig = null;

    private static boolean devMode = false;

    public static String getConfigValue(String key) {
        if (disCoreConfig == null) {
            initConfig();
        }
        return disCoreConfig.getProperty(key);
    }

    public static String formatConfigValue(String key, Object... args) {
        if (disCoreConfig == null) {
            initConfig();
        }
        return String.format(disCoreConfig.getProperty(key), args);
    }

    private static void initConfig() {
        disCoreConfig = new Properties();
        try {
            InputStream in;
            if (devMode) {
                in = new FileInputStream(configFileName);
            } else {
                in = DisCore.class.getResourceAsStream("/" + configFileName);
            }
            disCoreConfig.load(in);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public static void setDevMode(boolean devMode) {
        DisCore.devMode = devMode;
    }

    /**
     * Instanziierung verhindern
     */
    private DisCore() {
    }
}
