package de.nameistaken.discore.bot;

import de.nameistaken.discore.command.CommandManager;
import de.nameistaken.discore.command.message.Command;
import de.nameistaken.discore.command.message.CommandContainer;
import de.nameistaken.discore.command.message.CommandEvent;
import de.nameistaken.discore.command.message.HelpContainer;
import de.nameistaken.discore.command.preset.Help;
import de.nameistaken.discore.command.reaction.ReactionCommand;
import de.nameistaken.discore.command.reaction.ReactionCommandContainer;
import de.nameistaken.discore.command.reaction.ReactionCommandEvent;
import de.nameistaken.discore.command.slash.*;
import de.nameistaken.discore.events.EventContainer;
import de.nameistaken.discore.events.EventListener;
import de.nameistaken.discore.events.EventManager;
import de.nameistaken.discore.exceptions.DisCoreException;
import de.nameistaken.discore.exceptions.MethodAnnotationException;
import de.nameistaken.discore.util.DCLogger;
import de.nameistaken.discore.util.DisCoreUtils;
import de.nameistaken.discore.embeder.MessageEmbeder;
import de.nameistaken.discore.converter.ConverterManager;
import de.nameistaken.discore.matcher.ArgumentType;
import de.nameistaken.discore.matcher.Matcher;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.GenericEvent;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.requests.restaction.CommandListUpdateAction;
import net.dv8tion.jda.api.utils.MemberCachePolicy;

import javax.security.auth.login.LoginException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Klasse, die einen DiscordBot baut
 *
 * @author NameIsTaken
 * @version 12.02.2021
 * @see DiscordBot für das finale Produkt des Ganzen
 */
public abstract class DiscordBotBuilder {
    private final JDABuilder jdaBuilder;
    private final String commandPrefix;
    private final Set<EventContainer<?>> eventContainers;
    private final Set<CommandContainer> commands;
    private final Map<String, SlashCommandBuilder> slashCommandBuilder;
    private final Set<SlashCommandContainer> slashCommandContainers;
    private final Set<ReactionCommandContainer> reactionCommands;
    private final boolean forcePrefix;
    private final boolean deleteUnknown;
    private Status status;
    private final String defaultServerId;

    public DiscordBotBuilder(String botToken, String commandPrefix) {
        this(botToken, commandPrefix, null, true, false);
    }

    public DiscordBotBuilder(String botToken, String commandPrefix, String defaultServerId) {
        this(botToken, commandPrefix, defaultServerId, true, false);
    }

    public DiscordBotBuilder(String botToken, String commandPrefix, boolean forcePrefix, boolean deleteUnknown) {
        this(botToken, commandPrefix, null, forcePrefix, deleteUnknown);
    }

    public DiscordBotBuilder(String botToken, String commandPrefix, String defaultServerId, boolean forcePrefix, boolean deleteUnknown) {
        jdaBuilder = JDABuilder.createDefault(botToken);
        this.status = Status.BUILDER_CREATED;
        this.commandPrefix = commandPrefix;
        this.defaultServerId = defaultServerId;
        this.commands = ConcurrentHashMap.newKeySet();
        this.slashCommandBuilder = new ConcurrentHashMap<>();
        this.slashCommandContainers = ConcurrentHashMap.newKeySet();
        this.eventContainers = ConcurrentHashMap.newKeySet();
        this.reactionCommands = ConcurrentHashMap.newKeySet();
        this.forcePrefix = forcePrefix;
        this.deleteUnknown = deleteUnknown;
    }

    public final Status getStatus() {
        return status;
    }

    protected final void checkStatus(Status status) {
        if (this.status != status) {
            throw new IllegalStateException("Action not allowed during this stage of bot creation. Current status: " + this.status + ", required status for this action: " + status);
        }
    }

    public enum Status {
        BUILDER_CREATED, BUILDING_JDA, JDA_CREATED, STARTED,
    }

    public final DiscordBot build() {
        checkStatus(Status.BUILDER_CREATED);

        DCLogger.logWithDivider(DCLogger.Type.INFO, this, "Starting pre JDA actions");
        registerCommand(Help.class);
        preJDABuild();

        JDA jda;
        DCLogger.logWithDivider(DCLogger.Type.INFO, this, "Building JDA");

        //init commandmanager
        CommandManager commandManager = CommandManager.initInstance(commandPrefix, commands, slashCommandContainers, reactionCommands, forcePrefix, deleteUnknown);
        register(CommandManager.class, commandManager);

        status = Status.BUILDING_JDA;

        //init EventManager
        EventManager eventManager = EventManager.initInstance(eventContainers);
        jdaBuilder.addEventListeners(eventManager);

        //start actual jda build
        try {
            jda = jdaBuilder.build().awaitReady();
            jda.setAutoReconnect(true);
        } catch (LoginException | IllegalArgumentException | InterruptedException e) {
            throw new DisCoreException("Could not build JDA", e);
        }
        status = Status.JDA_CREATED;

        DCLogger.logWithDivider(DCLogger.Type.INFO, this, "JDA built, holding events");
        DiscordBot bot = DiscordBot.init(jda, commandPrefix, defaultServerId);
        postJDABuild(bot);

        //publish slash commands to discord and fire queued events
        registerSlashCommands(jda);
        eventManager.start();

        status = Status.STARTED;
        DCLogger.logWithDivider(DCLogger.Type.INFO, this, "Bot running");
        postBotStart(bot);

        return bot;
    }

    /**
     * hier müssen Commands, Listener etc registriert werden
     */
    protected abstract void preJDABuild();

    /**
     * Der Bot läuft hier technisch gesehen bereits, alle Events werden aber vom EventManager noch zurückgehalten.
     * Wenn also Dinge initialisiert werden müssen, die bereits die connection zur API brauchen (zB Channel anhand der ID finden o.ä.)
     * und ohne die zB Commands oder Events nicht richtig funktionieren/Fehler werfen etc, dann ist hier der perfekte Ort um diese Initalisierungen durchzuführen.
     *
     * @param bot DiscordBot Objekt mit dem zB auf das gebaute JDA objekt zugegriffen werden kann
     */
    protected abstract void postJDABuild(DiscordBot bot);

    /**
     * ab hier läuft der Bot in vollem Umfang. Alle vorher zurückgehaltenen Events wurden abgefeuert und neue Events werden sofort verwendet
     */
    protected abstract void postBotStart(DiscordBot bot);

    public final void defaultSettings() {
        checkStatus(Status.BUILDER_CREATED);
        enableIntents(
                GatewayIntent.GUILD_MEMBERS, GatewayIntent.GUILD_BANS, GatewayIntent.GUILD_PRESENCES, GatewayIntent.GUILD_MESSAGES, GatewayIntent.GUILD_MESSAGE_REACTIONS,
                GatewayIntent.DIRECT_MESSAGES, GatewayIntent.DIRECT_MESSAGE_REACTIONS
        );

        setMemberCachePolicy(MemberCachePolicy.ALL);
    }

    public final void enableIntents(GatewayIntent intent, GatewayIntent... intents) {
        //info: https://ci.dv8tion.net/job/JDA/javadoc/net/dv8tion/jda/api/requests/GatewayIntent.html
        checkStatus(Status.BUILDER_CREATED);
        jdaBuilder.enableIntents(intent, intents);
    }

    public final void disableIntents(GatewayIntent intent, GatewayIntent... intents) {
        //info: https://ci.dv8tion.net/job/JDA/javadoc/net/dv8tion/jda/api/requests/GatewayIntent.html
        checkStatus(Status.BUILDER_CREATED);
        jdaBuilder.disableIntents(intent, intents);
    }

    public final void setMemberCachePolicy(MemberCachePolicy policy) {
        checkStatus(Status.BUILDER_CREATED);
        jdaBuilder.setMemberCachePolicy(policy);
    }

    public final void register(Class<?> clazz) {
        register(clazz, null, (String[]) null);
    }

    public final void register(Class<?> clazz, Object invokeObject) {
        register(clazz, invokeObject, (String[]) null);
    }

    /**
     * @param clazz       Klasse, deren Methoden auf mit {@link Command}, {@link SlashCommand}, {@link ReactionCommand} sowie {@link EventListener} annotierte Methoden geprüft
     *                    und ggf als jeweiliger Command/Listener registriert werden soll
     * @param permissions Permissions, die für diese Commands gesetzt werden sollen (nur für Commands und SlashCommands)
     */
    public final void register(Class<?> clazz, Object invokeObject, String... permissions) {
        checkStatus(Status.BUILDER_CREATED);

        //try to instantiate commandClass
        DCLogger.log(DCLogger.Type.INFO, CommandManager.class, "Registering: " + clazz.getName());
        if (invokeObject == null) {
            try {
                invokeObject = clazz.newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                throw new DisCoreException("Unable to instantiate command class", e);
            }
        }

        //check Methods of listener class for valid Command annotation
        boolean registered = false;
        for (Method method : clazz.getMethods()) {
            if (method.isAnnotationPresent(Command.class)) {
                registered = true;
                registerCommand(method, invokeObject, permissions);
            } else if (method.isAnnotationPresent(SlashCommand.class)) {
                registered = true;
                registerSlashCommand(method, invokeObject, permissions);
            } else if (method.isAnnotationPresent(ReactionCommand.class)) {
                registered = true;
                registerReactionCommand(clazz, method, invokeObject);
            } else if (method.isAnnotationPresent(EventListener.class)) {
                registered = true;
                registerListener(method, invokeObject);
            }
        }

        if (!registered) {
            DCLogger.log(DCLogger.Type.WARN, CommandManager.class, "Class " + clazz.getSimpleName() + " does not contain any annotated command or listener methods");
        }
    }

    private void registerListener(Method method, Object invokeObject) {
        DCLogger.log(DCLogger.Type.INFO, EventManager.class, "Found event listener method: " + method.getName());
        if (method.getParameterCount() != 1) {
            throw new MethodAnnotationException("wrong parameter count");
        }
        Class<?> parameterClass = method.getParameterTypes()[0];
        if (!GenericEvent.class.isAssignableFrom(parameterClass)) {
            throw new MethodAnnotationException(String.format("invalid parameter class: %s is no subclass of GenericEvent", parameterClass.getName()));
        }
        eventContainers.add(new EventContainer(invokeObject, method, parameterClass));
    }

    private void registerReactionCommand(Class<?> clazz, Method method, Object invokeObject) {
        DCLogger.log(DCLogger.Type.INFO, this, "Found command method: " + method.getName());
        ReactionCommand annotation = method.getAnnotation(ReactionCommand.class);
        if (method.getParameterCount() != 1) {
            throw new MethodAnnotationException("wrong parameter count");
        } else if (!method.getParameters()[0].getType().equals(ReactionCommandEvent.class)) {
            throw new MethodAnnotationException("first argument must be of type ReactionCommandEvent");
        }

        reactionCommands.add(new ReactionCommandContainer(clazz, annotation, invokeObject, method));
    }

    private void registerCommand(Class<?> commandClass, String... permissions) {
        checkStatus(Status.BUILDER_CREATED);

        //try to instantiate commandClass
        DCLogger.log(DCLogger.Type.INFO, this, "Registering command: " + commandClass.getName());
        Object instance;
        try {
            instance = commandClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new DisCoreException("Unable to instantiate command class", e);
        }

        //check Methods of listener class for valid Command annotation
        boolean registered = false;
        for (Method method : commandClass.getMethods()) {
            if (method.isAnnotationPresent(Command.class)) {
                registered = true;
                registerCommand(method, instance, permissions);
            } else if (method.isAnnotationPresent(SlashCommand.class)) {
                registered = true;
                registerSlashCommand(method, instance, permissions);
            }
        }

        if (!registered) {
            DCLogger.log(DCLogger.Type.WARN, this, "Class " + commandClass.getSimpleName() + " does not contain any annotated command methods");
        }
    }

    private void registerCommand(Method method, Object instance, String[] permissions) {
        DCLogger.log(DCLogger.Type.INFO, this, "Found command method: " + method.getName());
        Command annotation = method.getAnnotation(Command.class);

        checkArguments(method, annotation.maxArgs(), false);

        //überprüfe Parameter der Command Methode auf zulässigen Typ
        if (!method.getParameters()[0].getType().equals(CommandEvent.class)) {
            throw new MethodAnnotationException("first argument must be of type CommandEvent");
        }
        Class<?>[] classes = Arrays.copyOfRange(method.getParameterTypes(), 1, method.getParameterCount());
        for (int i = 0; i < classes.length; i++) {
            Class<?> clazz = classes[i];
            if (!ConverterManager.isConverterAvailable(clazz)) {
                if (!(i == classes.length - 1 && clazz == String[].class)) {
                    //String[] als letztes Argument ist ne extrawurst
                    throw new MethodAnnotationException("cannot find converter for type " + clazz.getName());
                }
            }
        }

        MessageEmbed helpMessage = generateHelpMessage(method, annotation);
        CommandContainer container = new CommandContainer(annotation, instance, method, classes, helpMessage);

        commands.add(container);
        if (permissions != null) {
            container.setPermissions(permissions);
        }
    }

    private void registerSlashCommand(Method method, Object instance, String... permissions) {
        DCLogger.log(DCLogger.Type.INFO, this, "Found slashcommand method: " + method.getName());
        SlashCommand annotation = method.getAnnotation(SlashCommand.class);
        //testen ob Name zulässig
        check(annotation.name());

        checkArguments(method, annotation.maxArgs(), true);

        //überprüfe Parameter der Command Methode auf zulässigen Typ
        if (!method.getParameters()[0].getType().equals(SlashCommandEvent.class)) {
            throw new MethodAnnotationException("first argument must be of type SlashCommandEvent");
        }

        //beim überprüfen der restlichen Parameter wird gleichzeitig bereits das OptionData Objekt für JDA erstellt
        // daher muss zuerst nach Beschreibungselementen gesucht werden um diese ggf nutzen zu können
        Collection<ArgumentDescription> descriptions = null;
        if (method.isAnnotationPresent(ArgumentDescription.class)) {
            descriptions = Collections.singleton(method.getAnnotation(ArgumentDescription.class));
        } else if (method.isAnnotationPresent(DescriptionContainer.class)) {
            descriptions = Arrays.asList(method.getAnnotation(DescriptionContainer.class).value());
        }

        Parameter[] parameters = Arrays.copyOfRange(method.getParameters(), 1, method.getParameterCount());
        OptionData[] arguments = new OptionData[parameters.length];
        SlashCommandArgument<?>[] containerArguments = new SlashCommandArgument[parameters.length];
        for (int i = 0; i < parameters.length; i++) {
            Parameter parameter = parameters[i];
            Class<?> clazz = parameter.getType();
            OptionType type = getOptionType(clazz);

            String description = null;
            String name = null;
            if (descriptions != null) {
                int index = i + 1;
                ArgumentDescription element = descriptions.stream()
                        .filter(desc -> desc.parameterIndex() == index)
                        .findFirst()
                        .orElse(null);
                if (element != null) {
                    description = element.description();
                    if (element.name() != null && !element.name().isEmpty()) {
                        name = element.name();
                    }
                }
            }
            if (description == null) {
                description = parameter.getType().getSimpleName();
            }
            if (name == null) {
                name = parameter.getName();
            }

            containerArguments[i] = new SlashCommandArgument<>(name, clazz, i < annotation.minArgs());
            OptionData optionData = new OptionData(type, name, description, i < annotation.minArgs());
            if (clazz.isAssignableFrom(ChoiceElement.class)) {
                //zu diesem Zeitpunkt ist durch getOptionType() bereits sichergestellt, dass es sich um ein Enum handelt, wenn ChoiceElement verwendet wird
                ChoiceElement[] choiceElements = DisCoreUtils.getEnumValues(clazz);
                for (ChoiceElement choiceElement : choiceElements) {
                    optionData.addChoice(choiceElement.getName(), choiceElement.getValue());
                }
            }
            arguments[i] = optionData;
        }

        //art des Commands bestimmen und commandkey festlegen
        String parentName;
        String groupName = null;
        String key;
        SlashCommandType type;
        if (annotation.parent() != null && !annotation.parent().isEmpty()) {
            if (ArgumentType.SLASH_SUBCOMMAND.matches(annotation.parent())) {
                List<String> content = Matcher.getContent(annotation.parent(), ArgumentType.SLASH_SUBCOMMAND);
                parentName = content.get(0);
                groupName = content.get(1);
                check(parentName);
                check(groupName);
                key = parentName + "/" + groupName + "/" + annotation.name();
                type = SlashCommandType.GROUP_SUBCOMMAND;
            } else {
                parentName = annotation.parent();
                check(parentName);
                key = parentName + "/" + annotation.name();
                type = SlashCommandType.SUBCOMMAND;
            }
        } else {
            key = annotation.name();
            parentName = annotation.name();
            type = SlashCommandType.COMMAND;
        }

        SlashCommandBuilder builder = slashCommandBuilder.get(parentName);
        if (type == SlashCommandType.COMMAND) {
            if (builder != null) {
                throw new DisCoreException("there is already a command with the name \"" + parentName + "\"");
            }
            slashCommandBuilder.put(parentName, new SlashCommandBuilder(annotation, annotation.name(), annotation.description(), arguments));
        } else {
            if (builder == null) {
                builder = new SlashCommandBuilder(annotation, parentName);
                slashCommandBuilder.put(parentName, builder);
            }
            if (type == SlashCommandType.SUBCOMMAND) {
                builder.addSubCommand(annotation, annotation.name(), annotation.description(), arguments);
            } else {
                builder.addToGroup(annotation, groupName, annotation.name(), annotation.description(), arguments);
            }
        }

        SlashCommandContainer container = new SlashCommandContainer(annotation, instance, method, key, containerArguments);
        slashCommandContainers.add(container);
        if (permissions != null) {
            container.setPermissions(permissions);
        }
    }

    private void checkArguments(Method method, int maxArgs, boolean slash) {
        //überprüfe Parameter auf richtige Anzahl
        if (method.getParameterCount() < 1) {
            throw new MethodAnnotationException("wrong parameter count");
        } else if (maxArgs != -1 && method.getParameterCount() - 1 > maxArgs) {
            throw new MethodAnnotationException("maximum Arguments smaller than actual parameter count");
        } else if (maxArgs == -1 || method.getParameterCount() - 1 < maxArgs) {
            if (slash && method.getParameters()[method.getParameterCount() - 1].getType() != String.class) {
                throw new MethodAnnotationException("maximum Arguments larger than actual parameter count (use String as last parameter type to get a dynamic amount of arguments)");
            } else if (!slash && method.getParameters()[method.getParameterCount() - 1].getType() != String[].class) {
                throw new MethodAnnotationException("maximum Arguments larger than actual parameter count (use String[] as last parameter type to get a dynamic amount of arguments)");
            }
        }
    }

    private MessageEmbed generateHelpMessage(Method method, Command annotation) {
        MessageEmbeder embeder = new MessageEmbeder().yellow();
        embeder.setTitle("Hilfe zu " + commandPrefix + annotation.name());
        if (method.isAnnotationPresent(de.nameistaken.discore.command.message.Help.class)) {
            de.nameistaken.discore.command.message.Help help = method.getAnnotation(de.nameistaken.discore.command.message.Help.class);
            embeder.addField(commandPrefix + annotation.name() + " " + help.usage(), help.description(), false);
        } else if (method.isAnnotationPresent(HelpContainer.class)) {
            de.nameistaken.discore.command.message.Help[] help = method.getAnnotation(HelpContainer.class).value();
            for (de.nameistaken.discore.command.message.Help h : help) {
                embeder.addField(commandPrefix + annotation.name() + " " + h.usage(), h.description(), false);
            }
        } else {
            embeder.append("Zu diesem Command sind keine Informationen verfügbar");
        }
        if (annotation.aliasses().length != 0) {
            StringBuilder descriptionBuilder = new StringBuilder();
            for (int i = 0; i < annotation.aliasses().length; i++) {
                descriptionBuilder.append(commandPrefix).append(annotation.aliasses()[i]);
                if (i != annotation.aliasses().length - 1) {
                    descriptionBuilder.append(", ");
                }
            }
            embeder.addField("Aliasse", descriptionBuilder.toString(), false);
        }
        return embeder.build();
    }

    private OptionType getOptionType(Class<?> clazz) {
        if (clazz.isAssignableFrom(String.class)) {
            return OptionType.STRING;
        } else if (clazz == int.class || clazz.isAssignableFrom(Integer.class) ||
                clazz == short.class || clazz.isAssignableFrom(Short.class) ||
                clazz == byte.class || clazz.isAssignableFrom(Byte.class) ||
                clazz == long.class || clazz.isAssignableFrom(Long.class)) {
            return OptionType.INTEGER;
        } else if (clazz == boolean.class || clazz.isAssignableFrom(Boolean.class)) {
            return OptionType.BOOLEAN;
        } else if (clazz.isAssignableFrom(User.class) || clazz.isAssignableFrom(Member.class)) {
            return OptionType.USER;
        } else if (clazz.isAssignableFrom(GuildChannel.class)) {
            return OptionType.CHANNEL;
        } else if (clazz.isAssignableFrom(Role.class)) {
            return OptionType.ROLE;
        } else if (clazz.isAssignableFrom(IMentionable.class)) {
            return OptionType.MENTIONABLE;
        } else if (clazz.isAssignableFrom(ChoiceElement.class)) {
            if (!clazz.isAssignableFrom(Enum.class)) {
                throw new MethodAnnotationException("class " + clazz + " is a ChoiceElement but no Enum.");
            }
            return OptionType.STRING;
        } else {
            if (!ConverterManager.isConverterAvailable(clazz)) {
                throw new MethodAnnotationException("cannot find converter for type " + clazz.getName());
            }
            return OptionType.STRING;
        }
    }

    private void check(String commandName) {
        for (char c : commandName.toCharArray()) {
            if (Character.isAlphabetic(c) && !Character.isLowerCase(c)) {
                throw new DisCoreException("command name must be lower case");
            }
        }
        if (!Matcher.matches(commandName, ArgumentType.SLASH_COMMAND_NAME)) {
            throw new DisCoreException("command may only contain alphanumeric and dash characters and must be between 1 and 32 characters long");
        }
    }

    private void registerSlashCommands(JDA jda) {
        Map<String, Set<CommandData>> commandsByServer = new HashMap<>();
        final String GLOBAL = "GLOBAL";
        for (SlashCommandBuilder builder : slashCommandBuilder.values()) {
            CommandData data = builder.build();
            if (builder.getType() == SlashCommand.Type.SERVER) {
                if (builder.getServerIds().length == 0) {
                    String serverId = DiscordBot.getInstance().getDefaultServerId();
                    if (serverId == null) {
                        throw new IllegalStateException("Default server not initialized but tried to use it while trying to register slashcommand");
                    }
                    commandsByServer.computeIfAbsent(serverId, k -> new HashSet<>()).add(data);
                } else {
                    for (String serverId : builder.getServerIds()) {
                        commandsByServer.computeIfAbsent(serverId, k -> new HashSet<>()).add(data);
                    }
                }
            } else {
                commandsByServer.computeIfAbsent(GLOBAL, k -> new HashSet<>()).add(data);
            }
        }
        commandsByServer.forEach((id, data) -> {
            CommandListUpdateAction updateAction;
            if (id.equals(GLOBAL)) {
                updateAction = jda.updateCommands();
            } else {
                Guild guild = jda.getGuildById(id);
                if (guild == null) {
                    throw new IllegalStateException("could not find guild with id " + id + " while trying to register slashcommands");
                }
                updateAction = guild.updateCommands();
            }
            updateAction.addCommands(data).queue();
        });
    }
}
