package de.nameistaken.discore.bot;

import de.nameistaken.discore.events.EventManager;
import de.nameistaken.discore.util.DCLogger;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.MessageReaction;
import net.dv8tion.jda.api.entities.User;

/**
 * Klasse, die einen DiscordBot repräsentiert und Methoden rund um den Bot liefert
 *
 * @author NameIsTaken
 * @version 12.02.2021
 * @see DiscordBotBuilder zum Erstellen und Starten des Bots
 */
public class DiscordBot {
    private static DiscordBot instance;

    public static DiscordBot getInstance() {
        return instance;
    }

    static DiscordBot init(JDA jda, String commandPrefix, String defaultServerId) {
        instance = new DiscordBot(jda, commandPrefix, defaultServerId);
        return instance;
    }

    private final JDA jda;
    private final long started;
    private final String defaultServerId;
    private final String commandPrefix;

    private DiscordBot(JDA jda, String commandPrefix, String defaultServerId) {
        this.jda = jda;
        this.started = System.currentTimeMillis();
        this.commandPrefix = commandPrefix;
        this.defaultServerId = defaultServerId;
    }

    public void stop() {
        EventManager.getInstance().stop();

        DCLogger.logWithDivider(DCLogger.Type.INFO, this, "Stopping bot");
        jda.shutdown();
        DCLogger.logWithDivider(DCLogger.Type.INFO, this, "Bot stopped");
    }

    public Guild getDefaultServer() {
        if (defaultServerId != null) {
            return jda.getGuildById(defaultServerId);
        }
        return null;
    }

    public String getDefaultServerId() {
        return defaultServerId;
    }

    public JDA getJda() {
        return jda;
    }

    public String getCommandPrefix() {
        return commandPrefix;
    }

    public User getBotUser() {
        return jda.getSelfUser();
    }

    public void setPlaying(String gameName) {
        jda.getPresence().setActivity(Activity.playing(gameName));
    }

    public void setListening(String ListeningName) {
        jda.getPresence().setActivity(Activity.listening(ListeningName));
    }

    public void setWatching(String watchingName) {
        jda.getPresence().setActivity(Activity.watching(watchingName));
    }

    public void setStreaming(String streamName, String streamURL) {
        jda.getPresence().setActivity(Activity.streaming(streamName, streamURL));
    }

    public void setOnlineStatus(OnlineStatus status) {
        jda.getPresence().setStatus(status);
    }

    public boolean hasReacted(MessageReaction reaction) {
        return reaction.retrieveUsers().stream().filter(user -> user.getId().equals(getBotUser().getId())).count() >= 1;
    }

    /**
     * @return Zeit des Botstarts in Millisekunden
     */
    public long getTimeStarted() {
        return started;
    }

    public long getDiscordRestPing() {
        return jda.getRestPing().complete();
    }

    public long getWebsocketPing() {
        return jda.getGatewayPing();
    }
}
