package de.nameistaken.discore.matcher;

import java.util.ArrayList;
import java.util.List;

public final class Matcher {
    public static boolean matches(String arg, Matchable matchable) {
        return matchable.matches(arg);
    }

    /**
     * Liefert Liste aus getrennten Argumenten
     *
     * @param arg Argument
     * @return Liste
     */
    public static List<String> getContent(String arg, Matchable matchable) {
        if (matchable.getGroupNumbers() == null || matchable.getRegexPattern() == null) {
            return null;
        }
        List<String> content = new ArrayList<>();
        java.util.regex.Matcher matcher = matchable.getRegexPattern().matcher(arg);
        matcher.find();
        for (int groupNumber : matchable.getGroupNumbers()) {
            content.add(matcher.group(groupNumber));
        }
        return content;
    }

    public static List<String> getContentIfMatches(String arg, Matchable matchable) {
        if (matches(arg, matchable)) {
            return getContent(arg, matchable);
        }
        return null;
    }
}
