package de.nameistaken.discore.matcher;

import java.util.regex.Pattern;

public interface Matchable {
    boolean matches(String arg);

    int[] getGroupNumbers();

    String getRegexString();

    default Pattern getRegexPattern() {
        return Pattern.compile(getRegexString());
    }
}
