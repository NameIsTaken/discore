package de.nameistaken.discore.matcher;

import java.util.regex.Pattern;

public enum ArgumentType implements Matchable {
    SLASH_COMMAND_NAME("([\\w-]{1,32})",
            1),
    SLASH_SUBCOMMAND(SLASH_COMMAND_NAME.getRegexString() + "/" + SLASH_COMMAND_NAME.getRegexString(),
            1, 2),

    ID_DISCORD("([0-9]{18})",
            1),

    USER_DISPLAY("@(.+)",
            1),
    USER_ID("@" + ID_DISCORD.getRegexString(),
            1),
    USER_DISCORD_MENTION("<@!?" + ID_DISCORD.getRegexString() + ">",
            1),
    USER_WITH_DISCRIMINATOR("@(.+)#(\\d{4})",
            1, 2),


    CHANNEL_DISPLAY("#(.*)",
            1),
    /**
     * Channel im Format #id
     */
    CHANNEL_ID("#" + ID_DISCORD.getRegexString(),
            1),
    /**
     * Channel im Format <#id> (Discord Mention)
     */
    CHANNEL_DISCORD_MENTION("<" + CHANNEL_ID.getRegexString() + ">",
            1),

    ROLE_DISPLAY("@(.*)",
            1),
    ROLE_ID("@" + ID_DISCORD.getRegexString(),
            1),
    /**
     * Rolle im Format <@&id> (Discord Mention)
     */
    ROLE_DISCORD_MENTION("<@&" + ID_DISCORD.getRegexString() + ">",
            1),

    /**
     * Emote im Format <:name:id> (Discord Mention)
     */
    EMOTE_DISCORD_MENTION("<:(.+):(" + ID_DISCORD.getRegexString() + ")>",
            1, 2),

    /**
     * Datum im Format dd.mm.yy oder dd.mm.yyyy
     */
    DATE_NOTIME("(([0-2]?\\d)|(3[01]))\\.((0?[1-9])|(1[0-2]))\\.\\d?\\d?(\\d{2})",
            1, 4, 7),
    /**
     * Datum im Format dd.mm.yy-hh:mm oder dd.mm.yyyy-hh:mm
     */
    DATE_EXACT("(([0-2]?\\d)|(3[01]))\\.((0?[1-9])|(1[0-2]))\\.\\d?\\d?(\\d{2})-(([0-1]?\\d)|(2[0-3])):([0-5]\\d)",
            1, 4, 7, 8, 11),
    /**
     * Zeit Intervall im Format <Anzahl><m/h/d/w>
     */
    DATE_INTERVAL("(\\d+)([mhdw])",
            1, 2),
    /**
     * Uhrzeit im Format hh:mm
     */
    DATE_TIME("(([0-1]?\\d)|(2[0-3])):([0-5]\\d)",
            1, 4),

    /**
     * Link zu einer Discord Nachricht
     */
    MESSAGELINK("https:\\/\\/(\\w+\\.)?discordapp\\.com\\/channels\\/([0-9]{18})\\/([0-9]{18})\\/([0-9]{18})",
            2, 3, 4),

    TEXT("(.*)",
            1),

    UNDEFINED(null, (int[]) null);

    private final String regex;
    private final Pattern regexPattern;
    private final int[] groupnumbers;

    ArgumentType(String regex, int... groupnumbers) {
        this.regex = regex;
        if (regex != null) {
            regexPattern = Pattern.compile(regex);
        } else {
            regexPattern = null;
        }
        this.groupnumbers = groupnumbers;
    }

    @Override
    public boolean matches(String arg) {
        return arg.matches(regex);
    }

    @Override
    public int[] getGroupNumbers() {
        return groupnumbers;
    }

    @Override
    public String getRegexString() {
        return regex;
    }

    @Override
    public Pattern getRegexPattern() {
        return regexPattern;
    }
}
