package de.nameistaken.discore.exceptions;

public class DisCoreException extends RuntimeException {
    public DisCoreException() {
        super("DisCore Error");
    }

    public DisCoreException(String message) {
        super("DisCore Error: " + message);
    }

    public DisCoreException(String message, Throwable cause) {
        super("DisCore Error: " + message, cause);
    }
}
