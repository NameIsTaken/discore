package de.nameistaken.discore.exceptions;

public class MethodAnnotationException extends RuntimeException {
    public MethodAnnotationException(String message) {
        super("Invalid method annotated: " + message);
    }

    public MethodAnnotationException(String message, Throwable cause) {
        super("Invalid method annotated: " + message, cause);
    }

    public MethodAnnotationException(Throwable cause) {
        super(cause);
    }
}