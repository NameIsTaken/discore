package de.nameistaken.discore.embeder;

import de.nameistaken.discore.DisCore;
import de.nameistaken.discore.util.Emote;
import de.nameistaken.discore.exceptions.DisCoreException;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;

import java.awt.*;
import java.util.Properties;

/**
 * Klasse zum einfacheren Erstellen von Discord Embed Nachrichten
 *
 * @author NameIsTaken
 * @version 21.03.2020
 */
public class MessageEmbeder {
    private static String defaultFooterUrl = null;
    private static Color defaultColor = Color.BLACK;
    private static Properties messageProperties;

    public static void setMessageProperties(Properties messageProperties) {
        MessageEmbeder.messageProperties = messageProperties;
    }

    private static String getProperty(String key) {
        String message = null;
        if (messageProperties != null) {
            message = messageProperties.getProperty(key);
        }
        if (message == null) {
            message = DisCore.getConfigValue(key);
        }
        return message;
    }

    public static void setDefaultFooterUrl(String url) {
        defaultFooterUrl = url;
    }

    public static void setDefaultColor(Color defaultColor) {
        MessageEmbeder.defaultColor = defaultColor;
    }

    private EmbedBuilder jdaEmbedBuilder;
    private StringBuilder descriptionBuilder;
    private int fields;

    public MessageEmbeder() {
        init();
    }

    private void init() {
        jdaEmbedBuilder = new EmbedBuilder();
        descriptionBuilder = new StringBuilder();
        setColor(defaultColor);
        fields = 0;
    }

    /**
     * Baut aus allen gegebenen Werten das Embed mit dem Discord arbeiten kann.
     * Diese Methode muss außerhalb der API quasi nicht verwendet werden, da Channel und User zum senden von Nachrichten auch MessageEmbeder akzeptieren
     *
     * @return gebautes Embed
     */
    public MessageEmbed build() {
        jdaEmbedBuilder.setDescription(descriptionBuilder.toString());
        return jdaEmbedBuilder.build();
    }

    /**
     * resettet den Builder, alle vorhandenen Daten werden gelöscht
     */
    public void clear() {
        init();
    }

    public MessageEmbeder addField(String name, String text, boolean inline) {
        if (fields + 1 > DisCore.MESSAGE_EMBED_FIELD_LIMIT) {
            throw new DisCoreException("Amount of embed fields may not exceed " + DisCore.MESSAGE_EMBED_FIELD_LIMIT + " fields");
        }
        jdaEmbedBuilder.addField(name, text, inline);
        fields++;
        return this;
    }

    /**
     * @return der Footer Builder des Embeds, kann genutzt werden um weiteren Inhalt an den Footer, ggf formatiert, anzuhängen
     */
    public MessageEmbeder setFooter(String footerText) {
        jdaEmbedBuilder.setFooter(footerText);
        return this;
    }

    /**
     * @param url URL, die hinter dem Titel hinterlegt werden soll
     */
    public MessageEmbeder setTitle(String title, String url) {
        jdaEmbedBuilder.setTitle(title, url);
        return this;
    }

    public MessageEmbeder setTitle(String title) {
        setTitle(title, null);
        return this;
    }

    public MessageEmbeder setTitle(Object title) {
        setTitle(title.toString(), null);
        return this;
    }

    public MessageEmbeder setTitle(Object title, String url) {
        setTitle(title.toString(), url);
        return this;
    }

    public MessageEmbeder setTitleKey(String titleKey) {
        setTitle(getProperty(titleKey), null);
        return this;
    }

    public MessageEmbeder setTitleKey(String titleKey, String url) {
        setTitle(getProperty(titleKey), url);
        return this;
    }

    /**
     * Setzt die Farbe des Embeds auf rot und den Titel der Nachricht auf den in der config angegebenen Errormessage Namen
     */
    public MessageEmbeder errorPreset() {
        red();
        setTitle(DisCore.getConfigValue("message.error.title"));
        return this;
    }

    /**
     * Setzt die Farbe des Embeds auf rot und den Titel der Nachricht auf den in der config angegebenen Errormessage Namen sowie die Beschreibung auf die angegebene Beschreibung
     */
    public MessageEmbeder errorPreset(String description) {
        red();
        setTitle(DisCore.getConfigValue("message.error.title"));
        descriptionBuilder.append(description);
        return this;
    }

    public MessageEmbeder errorPresetKey(String descriptionKey) {
        red();
        setTitle(DisCore.getConfigValue("message.error.title"));
        descriptionBuilder.append(getProperty(descriptionKey));
        return this;
    }

    /**
     * Setzt die Farbe des Embeds auf grün und den Titel der Nachricht auf den white_check_mark Emote
     */
    public MessageEmbeder successPreset() {
        green();
        setTitle(Emote.WHITE_CHECK_MARK);
        return this;
    }

    public MessageEmbeder setAuthor(String author) {
        if (author != null) {
            jdaEmbedBuilder.setAuthor(author);
        }
        return this;
    }

    /**
     * @param authorURL URL des "Profilbilds" für die Nachricht
     */
    public MessageEmbeder setAuthor(String author, String authorURL) {
        if (author != null && authorURL != null) {
            jdaEmbedBuilder.setAuthor(author, authorURL, authorURL);
        }
        return this;
    }

    public MessageEmbeder setAuthor(String author, String clickURL, String imageURL) {
        if (author != null && clickURL != null && imageURL != null) {
            jdaEmbedBuilder.setAuthor(author, clickURL, imageURL);
        }
        return this;
    }

    /**
     * @param thumbnailURL URL des Bildes, das als Thumbnail genutzt werden soll (kleines Bild neben dem Titel der Nachricht)
     */
    public MessageEmbeder setThumbnail(String thumbnailURL) {
        if (thumbnailURL != null) {
            jdaEmbedBuilder.setThumbnail(thumbnailURL);
        }
        return this;
    }

    /**
     * @param imageUrl URL des Bildes, das genutzt werden soll (großes Bild unter der Nachricht)
     */
    public MessageEmbeder setImage(String imageUrl) {
        if (imageUrl != null) {
            jdaEmbedBuilder.setImage(imageUrl);
        }
        return this;
    }

    /**
     * Methode ist private, da außer den "Standard" farben keine Farben mit der JDA funktionieren
     */
    private void setColor(Color color) {
        jdaEmbedBuilder.setColor(color);
    }

    public MessageEmbeder blue() {
        setColor(Color.BLUE);
        return this;
    }

    public MessageEmbeder cyan() {
        setColor(Color.CYAN);
        return this;
    }

    public MessageEmbeder black() {
        setColor(Color.BLACK);
        return this;
    }

    public MessageEmbeder darkGray() {
        setColor(Color.DARK_GRAY);
        return this;
    }

    public MessageEmbeder gray() {
        setColor(Color.GRAY);
        return this;
    }

    public MessageEmbeder lightGray() {
        setColor(Color.LIGHT_GRAY);
        return this;
    }

    public MessageEmbeder white() {
        setColor(Color.WHITE);
        return this;
    }

    public MessageEmbeder green() {
        setColor(Color.GREEN);
        return this;
    }

    public MessageEmbeder magenta() {
        setColor(Color.MAGENTA);
        return this;
    }

    public MessageEmbeder orange() {
        setColor(Color.ORANGE);
        return this;
    }

    public MessageEmbeder pink() {
        setColor(Color.PINK);
        return this;
    }

    public MessageEmbeder red() {
        setColor(Color.RED);
        return this;
    }

    public MessageEmbeder yellow() {
        setColor(Color.YELLOW);
        return this;
    }


    public MessageEmbeder setDescription(String description) {
        descriptionBuilder = new StringBuilder();
        descriptionBuilder.append(description);
        return this;
    }

    public MessageEmbeder setDescriptionKey(String descriptionKey) {
        descriptionBuilder = new StringBuilder();
        descriptionBuilder.append(getProperty(descriptionKey));
        return this;
    }

    /* ============================== append - unformatiert ============================== */
    /* jegliche Append Methoden sind auf die Description des Embeds bezogen */

    public MessageEmbeder append(String description) {
        descriptionBuilder.append(description);
        return this;
    }

    public MessageEmbeder appendLink(String name, String url) {
        descriptionBuilder.append(String.format("[%s](%s)", name, url));
        return this;
    }

    public MessageEmbeder ln() {
        descriptionBuilder.append("\n");
        return this;
    }

    public MessageEmbeder append(char c) {
        return append(String.valueOf(c));
    }

    public MessageEmbeder append(boolean b) {
        return append(String.valueOf(b));
    }

    public MessageEmbeder append(int i) {
        return append(String.valueOf(i));
    }

    public MessageEmbeder append(long l) {
        return append(String.valueOf(l));
    }

    public MessageEmbeder append(float f) {
        return append(String.valueOf(f));
    }

    public MessageEmbeder append(double d) {
        return append(String.valueOf(d));
    }

    public MessageEmbeder append(Object obj) {
        return append(String.valueOf(obj));
    }

    /* ============================== append - formatiert ============================== */

    public MessageEmbeder append(String text, Format format) {
        return append(format.format(text));
    }

    /* ============================== append - formatiert - kursiv ============================== */

    public MessageEmbeder formatItalic(String text) {
        return append(Format.ITALIC.format(text));
    }

    public MessageEmbeder formatItalic(char c) {
        return formatItalic(String.valueOf(c));
    }

    public MessageEmbeder formatItalic(boolean b) {
        return formatItalic(String.valueOf(b));
    }

    public MessageEmbeder formatItalic(int i) {
        return formatItalic(String.valueOf(i));
    }

    public MessageEmbeder formatItalic(long l) {
        return formatItalic(String.valueOf(l));
    }

    public MessageEmbeder formatItalic(float f) {
        return formatItalic(String.valueOf(f));
    }

    public MessageEmbeder formatItalic(double d) {
        return formatItalic(String.valueOf(d));
    }

    public MessageEmbeder formatItalic(Object obj) {
        return formatItalic(String.valueOf(obj));
    }

    /* ============================== append - formatiert - fett ============================== */

    public MessageEmbeder formatBold(String text) {
        return append(Format.BOLD.format(text));
    }

    public MessageEmbeder formatBold(char c) {
        return formatBold(String.valueOf(c));
    }

    public MessageEmbeder formatBold(boolean b) {
        return formatBold(String.valueOf(b));
    }

    public MessageEmbeder formatBold(int i) {
        return formatBold(String.valueOf(i));
    }

    public MessageEmbeder formatBold(long l) {
        return formatBold(String.valueOf(l));
    }

    public MessageEmbeder formatBold(float f) {
        return formatBold(String.valueOf(f));
    }

    public MessageEmbeder formatBold(double d) {
        return formatBold(String.valueOf(d));
    }

    public MessageEmbeder formatBold(Object obj) {
        return formatBold(String.valueOf(obj));
    }

    /* ============================== append - formatiert - unterstrichen ============================== */

    public MessageEmbeder formatUnderlined(String text) {
        return append(Format.UNDERLINED.format(text));
    }

    public MessageEmbeder formatUnderlined(char c) {
        return formatUnderlined(String.valueOf(c));
    }

    public MessageEmbeder formatUnderlined(boolean b) {
        return formatUnderlined(String.valueOf(b));
    }

    public MessageEmbeder formatUnderlined(int i) {
        return formatUnderlined(String.valueOf(i));
    }

    public MessageEmbeder formatUnderlined(long l) {
        return formatUnderlined(String.valueOf(l));
    }

    public MessageEmbeder formatUnderlined(float f) {
        return formatUnderlined(String.valueOf(f));
    }

    public MessageEmbeder formatUnderlined(double d) {
        return formatUnderlined(String.valueOf(d));
    }

    public MessageEmbeder formatUnderlined(Object obj) {
        return formatUnderlined(String.valueOf(obj));
    }

    /* ============================== append - formatiert - fett kursiv ============================== */

    public MessageEmbeder formatBoldItalic(String text) {
        return append(Format.BOLD_ITALIC.format(text));
    }

    public MessageEmbeder formatBoldItalic(char c) {
        return formatBoldItalic(String.valueOf(c));
    }

    public MessageEmbeder formatBoldItalic(boolean b) {
        return formatBoldItalic(String.valueOf(b));
    }

    public MessageEmbeder formatBoldItalic(int i) {
        return formatBoldItalic(String.valueOf(i));
    }

    public MessageEmbeder formatBoldItalic(long l) {
        return formatBoldItalic(String.valueOf(l));
    }

    public MessageEmbeder formatBoldItalic(float f) {
        return formatBoldItalic(String.valueOf(f));
    }

    public MessageEmbeder formatBoldItalic(double d) {
        return formatBoldItalic(String.valueOf(d));
    }

    public MessageEmbeder formatBoldItalic(Object obj) {
        return formatBoldItalic(String.valueOf(obj));
    }

    /* ============================== append - formatiert - fett unterstrichen ============================== */

    public MessageEmbeder formatBoldUnderlined(String text) {
        return append(Format.BOLD_UNDERLINED.format(text));
    }

    public MessageEmbeder formatBoldUnderlined(char c) {
        return formatBoldUnderlined(String.valueOf(c));
    }

    public MessageEmbeder formatBoldUnderlined(boolean b) {
        return formatBoldUnderlined(String.valueOf(b));
    }

    public MessageEmbeder formatBoldUnderlined(int i) {
        return formatBoldUnderlined(String.valueOf(i));
    }

    public MessageEmbeder formatBoldUnderlined(long l) {
        return formatBoldUnderlined(String.valueOf(l));
    }

    public MessageEmbeder formatBoldUnderlined(float f) {
        return formatBoldUnderlined(String.valueOf(f));
    }

    public MessageEmbeder formatBoldUnderlined(double d) {
        return formatBoldUnderlined(String.valueOf(d));
    }

    public MessageEmbeder formatBoldUnderlined(Object obj) {
        return formatBoldUnderlined(String.valueOf(obj));
    }

    /* ============================== append - formatiert - fett unterstrichen kursiv ============================== */

    public MessageEmbeder formatBoldUnderlinedItalic(String text) {
        return append(Format.BOLD_UNDERLINED_ITALIC.format(text));
    }

    public MessageEmbeder formatBoldUnderlinedItalic(char c) {
        return formatBoldUnderlinedItalic(String.valueOf(c));
    }

    public MessageEmbeder formatBoldUnderlinedItalic(boolean b) {
        return formatBoldUnderlinedItalic(String.valueOf(b));
    }

    public MessageEmbeder formatBoldUnderlinedItalic(int i) {
        return formatBoldUnderlinedItalic(String.valueOf(i));
    }

    public MessageEmbeder formatBoldUnderlinedItalic(long l) {
        return formatBoldUnderlinedItalic(String.valueOf(l));
    }

    public MessageEmbeder formatBoldUnderlinedItalic(float f) {
        return formatBoldUnderlinedItalic(String.valueOf(f));
    }

    public MessageEmbeder formatBoldUnderlinedItalic(double d) {
        return formatBoldUnderlinedItalic(String.valueOf(d));
    }

    public MessageEmbeder formatBoldUnderlinedItalic(Object obj) {
        return formatBoldUnderlinedItalic(String.valueOf(obj));
    }

    /* ============================== append - formatiert - unterstrichen kursiv ============================== */

    public MessageEmbeder formatUnderlinedItalic(String text) {
        return append(Format.UNDERLINED_ITALIC.format(text));
    }

    public MessageEmbeder formatUnderlinedItalic(char c) {
        return formatUnderlinedItalic(String.valueOf(c));
    }

    public MessageEmbeder formatUnderlinedItalic(boolean b) {
        return formatUnderlinedItalic(String.valueOf(b));
    }

    public MessageEmbeder formatUnderlinedItalic(int i) {
        return formatUnderlinedItalic(String.valueOf(i));
    }

    public MessageEmbeder formatUnderlinedItalic(long l) {
        return formatUnderlinedItalic(String.valueOf(l));
    }

    public MessageEmbeder formatUnderlinedItalic(float f) {
        return formatUnderlinedItalic(String.valueOf(f));
    }

    public MessageEmbeder formatUnderlinedItalic(double d) {
        return formatUnderlinedItalic(String.valueOf(d));
    }

    public MessageEmbeder formatUnderlinedItalic(Object obj) {
        return formatUnderlinedItalic(String.valueOf(obj));
    }

    /* ============================== append - formatiert - durchgestrichen ============================== */

    public MessageEmbeder formatStrikethrough(String text) {
        return append(Format.STRIKETHROUGH.format(text));
    }

    public MessageEmbeder formatStrikethrough(char c) {
        return formatStrikethrough(String.valueOf(c));
    }

    public MessageEmbeder formatStrikethrough(boolean b) {
        return formatStrikethrough(String.valueOf(b));
    }

    public MessageEmbeder formatStrikethrough(int i) {
        return formatStrikethrough(String.valueOf(i));
    }

    public MessageEmbeder formatStrikethrough(long l) {
        return formatStrikethrough(String.valueOf(l));
    }

    public MessageEmbeder formatStrikethrough(float f) {
        return formatStrikethrough(String.valueOf(f));
    }

    public MessageEmbeder formatStrikethrough(double d) {
        return formatStrikethrough(String.valueOf(d));
    }

    public MessageEmbeder formatStrikethrough(Object obj) {
        return formatStrikethrough(String.valueOf(obj));
    }

    /* ============================== append - formatiert - codeline ============================== */

    public MessageEmbeder formatCodeLine(String text) {
        return append(Format.CODE_LINE.format(text));
    }

    public MessageEmbeder formatCodeLine(char c) {
        return formatCodeLine(String.valueOf(c));
    }

    public MessageEmbeder formatCodeLine(boolean b) {
        return formatCodeLine(String.valueOf(b));
    }

    public MessageEmbeder formatCodeLine(int i) {
        return formatCodeLine(String.valueOf(i));
    }

    public MessageEmbeder formatCodeLine(long l) {
        return formatCodeLine(String.valueOf(l));
    }

    public MessageEmbeder formatCodeLine(float f) {
        return formatCodeLine(String.valueOf(f));
    }

    public MessageEmbeder formatCodeLine(double d) {
        return formatCodeLine(String.valueOf(d));
    }

    public MessageEmbeder formatCodeLine(Object obj) {
        return formatCodeLine(String.valueOf(obj));
    }

    /* ============================== append - formatiert - codeblock ============================== */

    public MessageEmbeder formatCodeBlock(String text) {
        return append(Format.CODE_BLOCK.format(text));
    }

    public MessageEmbeder formatCodeBlock(char c) {
        return formatCodeBlock(String.valueOf(c));
    }

    public MessageEmbeder formatCodeBlock(boolean b) {
        return formatCodeBlock(String.valueOf(b));
    }

    public MessageEmbeder formatCodeBlock(int i) {
        return formatCodeBlock(String.valueOf(i));
    }

    public MessageEmbeder formatCodeBlock(long l) {
        return formatCodeBlock(String.valueOf(l));
    }

    public MessageEmbeder formatCodeBlock(float f) {
        return formatCodeBlock(String.valueOf(f));
    }

    public MessageEmbeder formatCodeBlock(double d) {
        return formatCodeBlock(String.valueOf(d));
    }

    public MessageEmbeder formatCodeBlock(Object obj) {
        return formatCodeBlock(String.valueOf(obj));
    }
}
