package de.nameistaken.discore.embeder;

public class ErrorEmbeder extends MessageEmbeder {
    public ErrorEmbeder() {
        super.errorPreset();
    }

    public MessageEmbeder notEnoughArgumentsError() {
        super.errorPresetKey("message.error.notenougharguments").ln();
        return this;
    }

    public MessageEmbeder tooManyArgumentsError() {
        super.errorPresetKey("message.error.toomanyarguments").ln();
        return this;
    }

    public MessageEmbeder noPrivateChatError() {
        super.errorPresetKey("message.error.noprivatechat").ln();
        return this;
    }

    public MessageEmbeder noPublicChatError() {
        super.errorPresetKey("message.error.nopublicchat").ln();
        return this;
    }

    public MessageEmbeder wrongChannelError() {
        super.errorPresetKey("message.error.wrongchannel").ln();
        return this;
    }

    public MessageEmbeder noPermissionError() {
        super.errorPresetKey("message.error.nopermission").ln();
        return this;
    }

    public MessageEmbeder invalidArgumentError() {
        super.errorPresetKey("message.error.invalidargument").ln();
        return this;
    }

    //========================= all Methods below are methods of the superclass that are not supported for this class =========================

    private void notSupported() {
        throw new UnsupportedOperationException();
    }

    @Override
    public MessageEmbeder setTitle(String title, String url) {
        notSupported();
        return null;
    }

    @Override
    public MessageEmbeder setTitle(String title) {
        notSupported();
        return null;
    }

    @Override
    public MessageEmbeder setTitle(Object title) {
        notSupported();
        return null;
    }

    @Override
    public MessageEmbeder setTitle(Object title, String url) {
        notSupported();
        return null;
    }

    @Override
    public MessageEmbeder setTitleKey(String titleKey) {
        notSupported();
        return null;
    }

    @Override
    public MessageEmbeder setTitleKey(String titleKey, String url) {
        notSupported();
        return null;
    }

    @Override
    public MessageEmbeder errorPreset() {
        notSupported();
        return null;
    }

    @Override
    public MessageEmbeder errorPreset(String description) {
        notSupported();
        return null;
    }

    @Override
    public MessageEmbeder errorPresetKey(String descriptionKey) {
        notSupported();
        return null;
    }

    @Override
    public MessageEmbeder successPreset() {
        notSupported();
        return null;
    }

    @Override
    public MessageEmbeder blue() {
        notSupported();
        return null;
    }

    @Override
    public MessageEmbeder cyan() {
        notSupported();
        return null;
    }

    @Override
    public MessageEmbeder black() {
        notSupported();
        return null;
    }

    @Override
    public MessageEmbeder darkGray() {
        notSupported();
        return null;
    }

    @Override
    public MessageEmbeder gray() {
        notSupported();
        return null;
    }

    @Override
    public MessageEmbeder lightGray() {
        notSupported();
        return null;
    }

    @Override
    public MessageEmbeder white() {
        notSupported();
        return null;
    }

    @Override
    public MessageEmbeder green() {
        notSupported();
        return null;
    }

    @Override
    public MessageEmbeder magenta() {
        notSupported();
        return null;
    }

    @Override
    public MessageEmbeder orange() {
        notSupported();
        return null;
    }

    @Override
    public MessageEmbeder pink() {
        notSupported();
        return null;
    }

    @Override
    public MessageEmbeder red() {
        notSupported();
        return null;
    }

    @Override
    public MessageEmbeder yellow() {
        notSupported();
        return null;
    }
}
