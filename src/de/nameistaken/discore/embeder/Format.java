package de.nameistaken.discore.embeder;

public enum Format {
    ITALIC("*"),
    BOLD("**"),
    BOLD_ITALIC("***"),
    BOLD_UNDERLINED("**__", "__**"),
    BOLD_UNDERLINED_ITALIC("**___", "___**"),
    UNDERLINED("__"),
    UNDERLINED_ITALIC("___"),
    STRIKETHROUGH("~~"),

    CODE_LINE("`"),
    CODE_BLOCK("```"),
    ;

    private final String prefix;
    private final String suffix;

    Format(String prefix, String suffix) {
        this.prefix = prefix;
        this.suffix = suffix;
    }

    Format(String fix) {
        prefix = fix;
        suffix = fix;
    }

    public String format(String text) {
        return prefix + text + suffix;
    }
}
