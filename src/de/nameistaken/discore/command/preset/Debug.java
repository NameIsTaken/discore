package de.nameistaken.discore.command.preset;

import de.nameistaken.discore.command.message.Command;
import de.nameistaken.discore.command.message.CommandEvent;
import de.nameistaken.discore.util.DCLogger;
import de.nameistaken.discore.embeder.MessageEmbeder;

public class Debug {

    @Command(
            name = "debug",
            maxArgs = 1,
            minArgs = 1
    )
    public void onDebug(CommandEvent event, boolean value) {
        DCLogger.setDebug(value);
        event.userMessenger().sendMessage(new MessageEmbeder().successPreset().append("Debug updated. Neuer Wert: ").append(value));
    }
}
