package de.nameistaken.discore.command.preset;

import de.nameistaken.discore.bot.DiscordBot;
import de.nameistaken.discore.command.message.Command;
import de.nameistaken.discore.command.message.CommandEvent;
import de.nameistaken.discore.command.message.Help;
import de.nameistaken.discore.util.DisCoreUtils;
import de.nameistaken.discore.embeder.MessageEmbeder;
import net.dv8tion.jda.api.entities.User;

public class Botinfo {

    @Command(
            name = "botinfo",
            maxArgs = 0
    )
    @Help(
            usage = "/botinfo",
            description = "Gibt Informationen zum Bot aus"
    )
    public void onBotinfo(CommandEvent event) {
        DiscordBot bot = DiscordBot.getInstance();
        User botUser = bot.getBotUser();

        MessageEmbeder embeder = new MessageEmbeder();
        embeder.setTitle("Informationen zum Bot");
        embeder.formatBold(botUser.getName()).append(" (").append(botUser.getAsTag()).append(")").ln()
                .formatBold("ID: ").append(botUser.getId()).ln()
                .ln()
                .formatBold("Current Servers: ").append(bot.getJda().getGuilds().size()).ln()
                .formatBold("Uptime: ").append(DisCoreUtils.formatTime(System.currentTimeMillis() - bot.getTimeStarted())).ln()
                .formatBold("Discord Rest Ping: ").append(bot.getDiscordRestPing()).ln()
                .formatBold("Websocket Ping: ").append(bot.getWebsocketPing());

        event.userMessenger().sendMessage(embeder);
    }
}
