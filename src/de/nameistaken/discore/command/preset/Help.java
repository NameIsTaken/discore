package de.nameistaken.discore.command.preset;

import de.nameistaken.discore.DisCore;
import de.nameistaken.discore.bot.DiscordBot;
import de.nameistaken.discore.command.CommandManager;
import de.nameistaken.discore.command.message.Command;
import de.nameistaken.discore.command.message.CommandContainer;
import de.nameistaken.discore.command.message.CommandEvent;
import de.nameistaken.discore.permission.PermissionManager;
import de.nameistaken.discore.embeder.MessageEmbeder;

@Deprecated //TODO /commands default command
public class Help {
    @Command(
            name = "help",
            maxArgs = 1
    )
    public void onHelp(CommandEvent event, String arg) {
        if (arg == null || arg.equalsIgnoreCase("all")) {
            sendAllCommandsHelpMessage(event);
        } else {
            CommandContainer command = CommandManager.getInstance().getCommandByName(arg);
            if (PermissionManager.hasPermission(event.getAuthor(), command.getPermissions())) {
                event.userMessenger().sendMessage(command.getMessage());
            } else {
                event.userMessenger().sendMessage(new MessageEmbeder().errorPreset().append(DisCore.getConfigValue("message.error.nopermission")));
            }
        }
    }

    private void sendAllCommandsHelpMessage(CommandEvent event) {
        MessageEmbeder embeder = new MessageEmbeder().yellow();
        embeder.setTitle("Verfügbare Commands");
        CommandManager.getInstance().getRegisteredCommands().forEach(command -> {
            if (PermissionManager.hasPermission(event.getAuthor(), command.getPermissions())) {
                embeder.formatBold(DiscordBot.getInstance().getCommandPrefix() + command.getName()).ln();
                if (command.getAliasses().length != 0) {
                    embeder.append("Aliasse: ");
                    for (String alias : command.getAliasses()) {
                        embeder.append(DiscordBot.getInstance().getCommandPrefix()).append(alias).append(" ");
                    }
                    embeder.ln();
                }
                embeder.ln();
            }
        });
        event.userMessenger().sendMessage(embeder);
    }
}
