package de.nameistaken.discore.command.preset;

import de.nameistaken.discore.bot.DiscordBot;
import de.nameistaken.discore.command.message.Command;
import de.nameistaken.discore.command.message.CommandEvent;
import de.nameistaken.discore.embeder.MessageEmbeder;

import java.util.Timer;
import java.util.TimerTask;

public class Shutdown {
    @Command(
            name = "stopbot",
            aliasses = {"shutdown"},
            maxArgs = 0
    )
    public void onStop(CommandEvent event) {
        event.userMessenger().sendMessage(new MessageEmbeder().successPreset().append("Der Bot wird gestoppt"));
        DiscordBot.getInstance().stop();
    }

    @Command(
            name = "stop",
            aliasses = {"exit"},
            maxArgs = 0
    )
    public void onShutdown(CommandEvent event) {
        event.userMessenger().sendMessage(new MessageEmbeder().successPreset().append("Der Bot und das Programm werden gestoppt"));
        DiscordBot.getInstance().stop();
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                Runtime.getRuntime().exit(0);
            }
        }, 500);
    }
}
