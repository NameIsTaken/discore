package de.nameistaken.discore.command.reaction;

import de.nameistaken.discore.util.Emote;
import de.nameistaken.discore.messenger.Messenger;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.react.GenericMessageReactionEvent;

public class ReactionCommandEvent {
    private final ChannelType type;
    private final User reactUser;
    private final Message message;
    private final MessageReaction reaction;
    private final Type reactionType;
    private final Emote reactionEmote;

    ReactionCommandEvent(GenericMessageReactionEvent event, Message message, MessageReaction reaction, Type reactionType, Emote reactionEmote) {
        this.type = event.getChannelType();
        this.reactUser = event.getUser();
        this.message = message;
        this.reaction = reaction;
        this.reactionType = reactionType;
        this.reactionEmote = reactionEmote;
    }

    public Messenger userMessenger() {
        return Messenger.get(reactUser);
    }

    public Messenger channelMessenger() {
        if (isFromType(ChannelType.PRIVATE)) {
            return userMessenger();
        } else {
            return Messenger.get(getChannel());
        }
    }

    public MessageChannel getChannel() {
        return message.getChannel();
    }

    public boolean isFromType(ChannelType type) {
        return this.type == type;
    }

    public User getReactUser() {
        return reactUser;
    }

    public Message getMessage() {
        return message;
    }

    public MessageReaction getReaction() {
        return reaction;
    }

    public Type getReactionType() {
        return reactionType;
    }

    public Emote getReactionEmote() {
        return reactionEmote;
    }

    public enum Type {
        ANY, REMOVE, ADD
    }
}
