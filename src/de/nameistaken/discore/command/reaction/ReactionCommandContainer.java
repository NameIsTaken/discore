package de.nameistaken.discore.command.reaction;

import de.nameistaken.discore.DisCore;
import de.nameistaken.discore.bot.DiscordBot;
import de.nameistaken.discore.util.Emote;
import de.nameistaken.discore.messenger.Messenger;
import de.nameistaken.discore.util.DCLogger;
import de.nameistaken.discore.permission.PermissionManager;
import de.nameistaken.discore.embeder.ErrorEmbeder;
import de.nameistaken.discore.embeder.MessageEmbeder;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageReaction;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.react.GenericMessageReactionEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionRemoveEvent;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReactionCommandContainer {
    private final ReactionCommand commandAnnotation;
    private final Object commandInstance;
    private final Method commandMethod;
    private final String[] permissions;
    private final String commandName;

    public ReactionCommandContainer(Class<?> commandClass, ReactionCommand commandAnnotation, Object commandInstance, Method commandMethod) {
        this.commandAnnotation = commandAnnotation;
        this.commandInstance = commandInstance;
        this.commandMethod = commandMethod;
        this.permissions = commandAnnotation.permissions();
        this.commandName = commandClass.getSimpleName() + "#" + commandMethod.getName();
    }

    public Emote[] getEmotes() {
        return commandAnnotation.emotes();
    }

    public boolean callCommand(GenericMessageReactionEvent event, Emote emote) {
        //checks zu Nachricht
        Message msg = event.getChannel().retrieveMessageById(event.getMessageId()).complete();
        if (msg == null) {
            return false;
        }

        //was jetzt passiert ist höchste Discord fuckup Kunst. Die API sendet beim Event nicht alle Infos der MessageReaction mit
        // daher versuchen wir die reaction nochmal getrennt von der Nachricht zu laden um volle Infos zu erhalten.
        // sollte im Normalfall klappen sofern nicht entweder die Nachricht zu schnell gelöscht wurde oder aber das Event das entfernen der letzten
        // vorhandenen Reaction repräsentiert. In dem Fall wird die unvollstände Reaction Information des Events genutzt
        MessageReaction reaction = msg.getReactions().stream().filter(r -> r.getReactionEmote().getName().equals(emote.toString())).findFirst().orElse(null);
        if (reaction == null) {
            reaction = event.getReaction();
        }

        if (commandAnnotation.needsBotMessage() && !msg.getAuthor().equals(DiscordBot.getInstance().getBotUser())) {
            return false;
        } else if (commandAnnotation.needsBotReaction() && !reaction.isSelf()) {
            return false;
        }
        ReactionCommandEvent.Type type;
        if (event instanceof MessageReactionAddEvent) {
            type = ReactionCommandEvent.Type.ADD;
        } else if (event instanceof MessageReactionRemoveEvent) {
            type = ReactionCommandEvent.Type.REMOVE;
        } else {
            throw new IllegalStateException();
        }
        if (commandAnnotation.reactionType() != ReactionCommandEvent.Type.ANY && commandAnnotation.reactionType() != type) {
            return false;
        }

        // =!= ab hier wird davon ausgegangen, dass die reaction als command gemeint war =!=
        // checks zu permissions, richtigem Channel etc.
        User reactor = event.getUser(); //reactor lol
        if (reactor == null) {
            throw new IllegalStateException(); //sollte nicht passieren, da das bei der Methode vorher schon gecheckt wird
        }
        Messenger messenger = Messenger.get(reactor);

        if (!PermissionManager.hasPermission(reactor, permissions)) {
            messenger.sendMessage(new ErrorEmbeder().noPermissionError());
            DCLogger.log(DCLogger.Type.INFO, this, reactor.getAsTag() + " failed to issue reactioncommand [NoPermission] " + commandName + " " + emote);
            return type == ReactionCommandEvent.Type.ADD; //removen falls es was zu removen gibt
        } else if (event.isFromType(ChannelType.PRIVATE) && !commandAnnotation.privateChannel()) {
            messenger.sendMessage(new ErrorEmbeder().noPrivateChatError());
            DCLogger.log(DCLogger.Type.INFO, this, reactor.getAsTag() + " failed to issue reactioncommand [NoPrivateChat] " + commandName + " " + emote);
            return type == ReactionCommandEvent.Type.ADD; //removen falls es was zu removen gibt
        } else if (event.isFromType(ChannelType.TEXT)) {
            if (commandAnnotation.serverChannel()) {
                if (commandAnnotation.channels().length != 0) {
                    boolean valid = false;
                    for (String channelId : commandAnnotation.channels()) {
                        if (channelId.equals(event.getChannel().getId())) {
                            valid = true;
                            break;
                        }
                    }
                    if (!valid) {
                        messenger.sendMessage(new ErrorEmbeder().wrongChannelError());
                        DCLogger.log(DCLogger.Type.INFO, this, reactor.getAsTag() + " failed to issue reactioncommand [WrongChannel] " + commandName + " " + emote);
                        return type == ReactionCommandEvent.Type.ADD; //removen falls es was zu removen gibt
                    }
                }
            } else {
                messenger.sendMessage(new ErrorEmbeder().noPublicChatError());
                DCLogger.log(DCLogger.Type.INFO, this, reactor.getAsTag() + " failed to issue reactioncommand [NoPublicChat] " + commandName + " " + emote);
                return type == ReactionCommandEvent.Type.ADD; //removen falls es was zu removen gibt
            }
        }
        ReactionCommandEvent commandEvent = new ReactionCommandEvent(event, msg, reaction, type, emote);

        // versuchen, Command auszuführen
        try {
            DCLogger.log(DCLogger.Type.INFO, this, reactor.getAsTag() + " issued command " + commandName + " " + emote);
            commandMethod.invoke(commandInstance, commandEvent);
        } catch (IllegalAccessException e) {
            messenger.sendMessage(new MessageEmbeder().errorPreset().append(String.format(DisCore.getConfigValue("message.error.veryunexpected"), e.getClass().getName())));
            DCLogger.log(DCLogger.Type.INFO, this, reactor.getAsTag() + " failed to issue command [IllegalAccessException] " + commandName + " " + emote);
            e.printStackTrace();
            return type == ReactionCommandEvent.Type.ADD; //removen falls es was zu removen gibt
        } catch (InvocationTargetException e) {
            messenger.sendMessage(new MessageEmbeder().errorPreset().append(String.format(DisCore.getConfigValue("message.error.unexpected"), e.getCause().getClass().getName())));
            DCLogger.log(DCLogger.Type.INFO, this, reactor.getAsTag() + " failed to issue command [InvocationTargetException] " + commandName + " " + emote);
            e.getCause().printStackTrace();
            return type == ReactionCommandEvent.Type.ADD; //removen falls es was zu removen gibt
        }
        return type == ReactionCommandEvent.Type.ADD && commandAnnotation.removeReaction(); //removen falls es was zu removen gibt
    }
}
