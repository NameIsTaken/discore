package de.nameistaken.discore.command.reaction;

import de.nameistaken.discore.util.Emote;
import de.nameistaken.discore.permission.PermissionHandler;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Reaction Command Annotation
 *
 * @author NameIsTaken
 * @version 17.02.2021
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ReactionCommand {
    Emote[] emotes();

    ReactionCommandEvent.Type reactionType() default ReactionCommandEvent.Type.ANY;

    String[] permissions() default {PermissionHandler.EVERYONE_PERMISSION};

    /**
     * Darf der Command im Privatchat mit dem Bot ausgeführt werden?
     */
    boolean privateChannel() default true;

    /**
     * Darf der Command in einem öffentlichen Channel ausgeführt werden?
     */
    boolean serverChannel() default true;

    /**
     * @return Channel IDs der Channel, in denen der Command ausgeführt werden darf (geht nur wenn serverChannel() true. Wenn leer darf der Command in allen Channeln ausgeführt werden)
     */
    String[] channels() default {};

    boolean removeReaction() default true;

    boolean needsBotMessage() default true;

    boolean needsBotReaction() default true;
}
