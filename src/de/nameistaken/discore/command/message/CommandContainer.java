package de.nameistaken.discore.command.message;

import de.nameistaken.discore.DisCore;
import de.nameistaken.discore.util.Result;
import de.nameistaken.discore.messenger.Messenger;
import de.nameistaken.discore.util.DCLogger;
import de.nameistaken.discore.permission.PermissionManager;
import de.nameistaken.discore.embeder.MessageEmbeder;
import de.nameistaken.discore.converter.ConverterManager;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * @author NameIsTaken
 * @version 24.03.2020
 */
public class CommandContainer {
    private final Command commandAnnotation;
    private final Object commandInstance;
    private final Method commandMethod;
    private final Class<?>[] argumentClasses;
    private String[] permissions;
    private final MessageEmbed message;

    public CommandContainer(Command commandAnnotation, Object commandInstance, Method commandMethod, Class<?>[] argumentClasses, MessageEmbed message) {
        this.commandAnnotation = commandAnnotation;
        this.commandInstance = commandInstance;
        this.commandMethod = commandMethod;
        this.argumentClasses = argumentClasses;
        this.permissions = commandAnnotation.permissions();
        this.message = message;
    }

    public void setPermissions(String... permissions) {
        this.permissions = permissions;
    }

    public String[] getPermissions() {
        return permissions;
    }

    public boolean isHelpMessageAvailable() {
        return message != null;
    }

    public String getName() {
        return commandAnnotation.name();
    }

    public String[] getAliasses() {
        return commandAnnotation.aliasses();
    }

    public MessageEmbed getMessage() {
        return message;
    }

    /**
     * @param event Command Nachricht Event
     * @param args  Argumente der Nachricht
     * @return true, falls die Nachricht des Commands gelöscht werden soll, false sonst
     */
    public boolean callCommand(MessageReceivedEvent event, String[] args) {
        // checks zu permissions, richtigem Channel etc.
        User author = event.getAuthor();
        Messenger messenger = Messenger.get(author);

        if (!PermissionManager.hasPermission(author, permissions)) {
            messenger.sendMessage(new MessageEmbeder().errorPreset().append(DisCore.getConfigValue("message.error.nopermission")));
            DCLogger.log(DCLogger.Type.INFO, this, event.getAuthor().getAsTag() + " failed to issue command [NoPermission] " + event.getMessage().getContentRaw());
            return true;
        } else if (args.length == 1 && args[0].equalsIgnoreCase("help")) {
            //help
            messenger.sendMessage(message);
            return true;
        }
        if (event.isFromType(ChannelType.PRIVATE) && !commandAnnotation.privateChannel()) {
            messenger.sendMessage(new MessageEmbeder().errorPreset().append(DisCore.getConfigValue("message.error.noprivatechat")));
            DCLogger.log(DCLogger.Type.INFO, this, event.getAuthor().getAsTag() + " failed to issue command [NoPrivateChat] " + event.getMessage().getContentRaw());
            return true;
        } else if (event.isFromType(ChannelType.TEXT)) {
            if (commandAnnotation.serverChannel()) {
                if (commandAnnotation.channels().length != 0) {
                    boolean valid = false;
                    for (String channelId : commandAnnotation.channels()) {
                        if (channelId.equals(event.getMessage().getTextChannel().getId())) {
                            valid = true;
                            break;
                        }
                    }
                    if (!valid) {
                        messenger.sendMessage(new MessageEmbeder().errorPreset().append(DisCore.getConfigValue("message.error.wrongchannel")));
                        DCLogger.log(DCLogger.Type.INFO, this, event.getAuthor().getAsTag() + " failed to issue command [WrongChannel] " + event.getMessage().getContentRaw());
                        return true;
                    }
                }
            } else {
                messenger.sendMessage(new MessageEmbeder().errorPreset().append(DisCore.getConfigValue("message.error.nopublicchat")));
                DCLogger.log(DCLogger.Type.INFO, this, event.getAuthor().getAsTag() + " failed to issue command [NoPublicChat] " + event.getMessage().getContentRaw());
                return true;
            }
        }

        // checks zur Argumentzahl
        if (commandAnnotation.minArgs() != -1 && args.length < commandAnnotation.minArgs()) {
            messenger.sendMessage(new MessageEmbeder().errorPreset().append(DisCore.getConfigValue("message.error.notenougharguments")));
            DCLogger.log(DCLogger.Type.INFO, this, event.getAuthor().getAsTag() + " failed to issue command [NotEnoughArguments] " + event.getMessage().getContentRaw());
            return true;
        } else if (commandAnnotation.maxArgs() != -1 && args.length > commandAnnotation.maxArgs()) {
            messenger.sendMessage(new MessageEmbeder().errorPreset().append(DisCore.getConfigValue("message.error.toomanyarguments")));
            DCLogger.log(DCLogger.Type.INFO, this, event.getAuthor().getAsTag() + " failed to issue command [TooManyArguments] " + event.getMessage().getContentRaw());
            return true;
        }

        Object[] arguments = new Object[argumentClasses.length + 1];
        arguments[0] = new CommandEvent(event, messenger);
        for (int i = 0; i < args.length && i < argumentClasses.length; i++) {
            if ((i == args.length - 1 || i == argumentClasses.length - 1) && argumentClasses[i] == String[].class) {
                // falls letztes Argument der Methode String array -> alle verbleibenden Argumente des Commands in ein neues String Array
                String[] leftargs = Arrays.copyOfRange(args, i, args.length);
                arguments[i + 1] = leftargs;
                break;
            }
            String arg = args[i];
            Class<?> argumentClass = argumentClasses[i];
            Result<?> result = ConverterManager.convertTo(arg, argumentClass);
            if (result.isValid()) {
                // Argument konnte in Instanz der angegebenen Klasse umgewandelt werden
                arguments[i + 1] = result.getResult();
            } else {
                // Fehlermeldung des Ergebnisses an User schicken
                messenger.sendMessage(result.errorEmbedBuilder());
                DCLogger.log(DCLogger.Type.INFO, this, event.getAuthor().getAsTag() + " failed to issue command [ConverterError] " + event.getMessage().getContentRaw());
                return true;
            }
        }

        // versuchen, Command auszuführen
        try {
            DCLogger.log(DCLogger.Type.INFO, this, event.getAuthor().getAsTag() + " issued command " + event.getMessage().getContentRaw());
            commandMethod.invoke(commandInstance, arguments);
        } catch (IllegalAccessException e) {
            messenger.sendMessage(new MessageEmbeder().errorPreset().append(String.format(DisCore.getConfigValue("message.error.veryunexpected"), e.getClass().getName())));
            DCLogger.log(DCLogger.Type.INFO, this, event.getAuthor().getAsTag() + " failed to issue command [IllegalAccessException] " + event.getMessage().getContentRaw());
            e.printStackTrace();
            return true;
        } catch (InvocationTargetException e) {
            messenger.sendMessage(new MessageEmbeder().errorPreset().append(String.format(DisCore.getConfigValue("message.error.unexpected"), e.getCause().getClass().getName())));
            DCLogger.log(DCLogger.Type.INFO, this, event.getAuthor().getAsTag() + " failed to issue command [InvocationTargetException] " + event.getMessage().getContentRaw());
            e.getCause().printStackTrace();
            return true;
        }

        return commandAnnotation.deleteCommandMessage();
    }
}
