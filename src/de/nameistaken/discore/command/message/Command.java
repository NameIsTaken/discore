package de.nameistaken.discore.command.message;

import de.nameistaken.discore.permission.PermissionHandler;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Command Annotation
 *
 * @author NameIsTaken
 * @version 21.03.2020
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Command {
    String name();

    String[] aliasses() default {};

    String[] permissions() default {PermissionHandler.EVERYONE_PERMISSION};

    int minArgs() default 0;

    int maxArgs() default -1;

    /**
     * Darf der Command im Privatchat mit dem Bot ausgeführt werden?
     */
    boolean privateChannel() default true;

    /**
     * Darf der Command in einem öffentlichen Channel ausgeführt werden?
     */
    boolean serverChannel() default false;

    /**
     * @return Channel IDs der Channel, in denen der Command ausgeführt werden darf (geht nur wenn serverChannel() true. Wenn leer darf der Command in allen Channeln ausgeführt werden)
     */
    String[] channels() default {};

    boolean deleteCommandMessage() default true;
}
