package de.nameistaken.discore.command.message;

import java.lang.annotation.*;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(HelpContainer.class)
public @interface Help {
    String usage();

    String description();
}
