package de.nameistaken.discore.command.message;

import de.nameistaken.discore.bot.DiscordBot;
import de.nameistaken.discore.messenger.Messenger;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class CommandEvent {
    private final MessageReceivedEvent event;
    private final Messenger authorMessenger;

    CommandEvent(MessageReceivedEvent event, Messenger authorMessenger) {
        this.event = event;
        this.authorMessenger = authorMessenger;
    }

    public Messenger userMessenger() {
        return authorMessenger;
    }

    public Message getMessage() {
        return event.getMessage();
    }

    public User getAuthor() {
        return event.getAuthor();
    }

    public Member getMember() {
        return getMember(DiscordBot.getInstance().getDefaultServer());
    }

    public Member getMember(Guild guild) {
        if (guild.isMember(event.getAuthor())) {
            return guild.getMember(event.getAuthor());
        } else {
            return null;
        }
    }

    public MessageChannel getChannel() {
        return event.getChannel();
    }

    public Messenger channelMessenger() {
        if (isFromType(ChannelType.PRIVATE)) {
            return userMessenger();
        } else {
            return Messenger.get(getChannel());
        }
    }

    public String getMessageId() {
        return event.getMessageId();
    }

    public boolean isFromType(ChannelType type) {
        return event.isFromType(type);
    }
}
