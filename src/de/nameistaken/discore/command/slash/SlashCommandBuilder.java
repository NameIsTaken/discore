package de.nameistaken.discore.command.slash;

import de.nameistaken.discore.exceptions.DisCoreException;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import net.dv8tion.jda.api.interactions.commands.build.SubcommandData;
import net.dv8tion.jda.api.interactions.commands.build.SubcommandGroupData;

import java.util.*;

public class SlashCommandBuilder {
    private final SlashCommandData data;
    private final Map<String, Set<SlashCommandData>> groups;
    private final Set<SlashCommandData> subcommands;
    private final boolean command;
    private SlashCommand.Type type;
    private String[] serverIds;

    public SlashCommandBuilder(SlashCommand annotation, String parentName) {
        this(annotation, new SlashCommandData(parentName, parentName, null), false);
    }

    public SlashCommandBuilder(SlashCommand annotation, String name, String description, OptionData[] options) {
        this(annotation, new SlashCommandData(name, description, options));
    }

    public SlashCommandBuilder(SlashCommand annotation, SlashCommandData data) {
        this(annotation, data, true);
    }

    private SlashCommandBuilder(SlashCommand annotation, SlashCommandData data, boolean command) {
        this.data = data;
        this.command = command;
        this.groups = new HashMap<>();
        this.subcommands = new HashSet<>();
        set(annotation);
    }

    public CommandData build() {
        Objects.requireNonNull(type);
        Objects.requireNonNull(serverIds);

        CommandData result;
        if (command) {
            result = new CommandData(data.getName(), data.getDescription());
            result.addOptions(data.getOptions());
        } else {
            result = new CommandData(data.getName(), data.getName());
            groups.forEach((groupName, commands) -> {
                SubcommandGroupData groupData = new SubcommandGroupData(groupName, groupName);
                for (SlashCommandData slashCommandData : commands) {
                    SubcommandData subcommandData = new SubcommandData(slashCommandData.getName(), slashCommandData.getDescription());
                    subcommandData.addOptions(slashCommandData.getOptions());
                    groupData.addSubcommands(subcommandData);
                }
                result.addSubcommandGroups(groupData);
            });
            for (SlashCommandData slashCommandData : subcommands) {
                SubcommandData subcommandData = new SubcommandData(slashCommandData.getName(), slashCommandData.getDescription());
                subcommandData.addOptions(slashCommandData.getOptions());
                result.addSubcommands(subcommandData);
            }
        }
        return result;
    }

    public void addToGroup(SlashCommand annotation, String groupName, String name, String description, OptionData[] options) {
        check();
        set(annotation);
        Set<SlashCommandData> group = groups.computeIfAbsent(groupName, k -> new HashSet<>());
        group.add(new SlashCommandData(name, description, options));
    }

    public void addSubCommand(SlashCommand annotation, String name, String description, OptionData[] options) {
        check();
        set(annotation);
        subcommands.add(new SlashCommandData(name, description, options));
    }

    private void check() {
        if (command) {
            throw new DisCoreException("cant add subcommands or subcommand groups to existing command");
        }
    }

    private void set(SlashCommand annotation) {
        if (annotation.useType()) {
            type = annotation.type();
            serverIds = annotation.serverIds();
        }
    }

    public SlashCommand.Type getType() {
        return type;
    }

    public String[] getServerIds() {
        return serverIds;
    }
}
