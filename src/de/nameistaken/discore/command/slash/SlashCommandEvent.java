package de.nameistaken.discore.command.slash;

import de.nameistaken.discore.bot.DiscordBot;
import de.nameistaken.discore.messenger.ChannelMessenger;
import de.nameistaken.discore.messenger.InteractionMessenger;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.interactions.InteractionHook;

public class SlashCommandEvent {
    private final net.dv8tion.jda.api.events.interaction.SlashCommandEvent discordEvent;
    private final InteractionHook hook;
    private final InteractionMessenger messenger;
    private final ChannelMessenger channelMessenger;

    public SlashCommandEvent(net.dv8tion.jda.api.events.interaction.SlashCommandEvent discordEvent, InteractionHook hook) {
        this.discordEvent = discordEvent;
        this.hook = hook;
        this.messenger = new InteractionMessenger(hook);
        this.channelMessenger = new ChannelMessenger(discordEvent.getMessageChannel());
    }

    public InteractionMessenger messenger() {
        return messenger;
    }

    public ChannelMessenger channelMessenger() {
        return channelMessenger;
    }

    public User getAuthor() {
        return discordEvent.getUser();
    }

    public Member getMember() {
        return getMember(DiscordBot.getInstance().getDefaultServer());
    }

    public Member getMember(Guild guild) {
        if (guild.isMember(getAuthor())) {
            return guild.getMember(getAuthor());
        } else {
            return null;
        }
    }

    public MessageChannel getChannel() {
        return discordEvent.getMessageChannel();
    }
}
