package de.nameistaken.discore.command.slash;

import net.dv8tion.jda.api.interactions.commands.build.OptionData;

public class SlashCommandData {
    private final String name;
    private final String description;
    private final OptionData[] options;

    public SlashCommandData(String name, String description, OptionData[] options) {
        this.name = name;
        this.description = description;
        this.options = options;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public OptionData[] getOptions() {
        return options;
    }
}
