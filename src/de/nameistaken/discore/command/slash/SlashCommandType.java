package de.nameistaken.discore.command.slash;

public enum SlashCommandType {
    COMMAND, GROUP_SUBCOMMAND, SUBCOMMAND
}
