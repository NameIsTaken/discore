package de.nameistaken.discore.command.slash;

/**
 * Interface was genutzt werden kann, um Auswahlmöglichkeiten für ein Argument eines Slashcommands festzulegen.
 * Muss als Enum implementiert werden! Sonst weiß das Programm nicht, welche Möglichkeiten es geben kann
 *
 * @author NameIsTaken
 * @version 12.10.2021
 */
public interface ChoiceElement {
    String getName();

    String getValue(); //könnte theoretisch auch Integer sein, lassen wir aber gut sein
}
