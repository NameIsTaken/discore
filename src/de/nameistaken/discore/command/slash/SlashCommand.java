package de.nameistaken.discore.command.slash;

import de.nameistaken.discore.bot.DiscordBot;
import de.nameistaken.discore.permission.PermissionHandler;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Command Annotation for Discords Slash Commands
 *
 * @author NameIsTaken
 * @version 09.08.2021
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface SlashCommand {
    /**
     * Wenn die Methode einen Subcommand darstellen soll, dann ist dies der Name des Parent Commands
     * Bsp:
     * /permission add|remove
     * -> für eine add Methode wäre parent = "permission"
     * <p>
     * Wenn der Command subcommand einer Commandgruppe sein soll muss der name des Parent-Parent Commands und der Name des
     * Parent Commands angegeben werden
     * Bsp:
     * /permission user add|remove
     * -> für eine add Methode wäre parent = "permission/user"
     */
    String parent() default "";

    String name();

    String description();

    String[] permissions() default {PermissionHandler.EVERYONE_PERMISSION};

    int minArgs() default 0;

    int maxArgs() default -1;

    /**
     * Darf der Command im Privatchat mit dem Bot ausgeführt werden? Funktioniert nur, wenn type() auf global steht
     */
    boolean privateChannel() default true;

    /**
     * Darf der Command in einem öffentlichen Channel ausgeführt werden?
     */
    boolean serverChannel() default true;

    /**
     * @return Channel IDs der Channel, in denen der Command ausgeführt werden darf (geht nur wenn serverChannel() true. Wenn leer darf der Command in allen Channeln ausgeführt werden)
     */
    String[] channels() default {};

    /**
     * @return wenn dies ein einzelner Command ohne subcommands ist, immer true! Sonst true bei der Methode, deren Werte von type() sowie ggf serverIds() zur Registrierung
     * des gesamten(!) Commands verwendet werden sollen. Wird useType nirgends true wird bei der Initialisierung des Commands ein Fehler auftreten
     */
    boolean useType();

    Type type() default Type.SERVER;

    /**
     * {@link DiscordBot#getDefaultServer()} wird per default verwendet
     */
    String[] serverIds() default {};

    enum Type {
        /**
         * Command auf allen Servern und im Privatchat mit dem Bot verfügbar
         */
        GLOBAL,

        /**
         * Command nur auf festgelegten Servern verfügbar
         */
        SERVER
    }
}
