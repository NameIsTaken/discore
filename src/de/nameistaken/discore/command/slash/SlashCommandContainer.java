package de.nameistaken.discore.command.slash;

import de.nameistaken.discore.DisCore;
import de.nameistaken.discore.util.Result;
import de.nameistaken.discore.util.DCLogger;
import de.nameistaken.discore.permission.PermissionManager;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.InteractionHook;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class SlashCommandContainer {
    private final SlashCommand commandAnnotation;
    private final Object commandInstance;
    private final Method commandMethod;

    private final String key;
    private final SlashCommandArgument<?>[] arguments;
    private String[] permissions;

    public SlashCommandContainer(SlashCommand commandAnnotation, Object commandInstance, Method commandMethod, String key, SlashCommandArgument<?>[] arguments) {
        this.commandAnnotation = commandAnnotation;
        this.commandInstance = commandInstance;
        this.commandMethod = commandMethod;
        this.key = key;
        this.arguments = arguments;
        permissions = commandAnnotation.permissions();
    }

    public String getKey() {
        return key;
    }

    public void setPermissions(String[] permissions) {
        this.permissions = permissions;
    }

    public void callCommand(SlashCommandEvent event, InteractionHook hook) {
        User author = event.getUser();

        //checks zu permissions, richtigem channel etc
        if (!PermissionManager.hasPermission(author, permissions)) {
            event.getHook().sendMessage(DisCore.getConfigValue("message.error.nopermission")).queue();
            DCLogger.log(DCLogger.Type.INFO, this, author.getAsTag() + " failed to issue slashcommand [NoPermission] " + event.getCommandPath());
            return;
        } else if (event.isFromGuild()) {
            if (commandAnnotation.type() == SlashCommand.Type.GLOBAL && !commandAnnotation.serverChannel()) {
                event.getHook().sendMessage(DisCore.getConfigValue("message.error.nopublicchat")).queue();
                DCLogger.log(DCLogger.Type.INFO, this, author.getAsTag() + " failed to issue slashcommand [NoPublicChat] " + event.getCommandPath());
                return;
            } else if (commandAnnotation.channels() != null && commandAnnotation.channels().length > 0 && !containsChannel(commandAnnotation.channels(), event.getChannel().getId())) {
                event.getHook().sendMessage(DisCore.getConfigValue("message.error.wrongchannel")).queue();
                DCLogger.log(DCLogger.Type.INFO, this, author.getAsTag() + " failed to issue slashcommand [WrongChannel] " + event.getCommandPath());
                return;
            }
        } else if (!commandAnnotation.privateChannel()) {
            event.getHook().sendMessage(DisCore.getConfigValue("message.error.noprivatechat")).queue();
            DCLogger.log(DCLogger.Type.INFO, this, author.getAsTag() + " failed to issue slashcommand [NoPrivateChat] " + event.getCommandPath());
            return;
        }

        //argumente für methode sammeln
        de.nameistaken.discore.command.slash.SlashCommandEvent commandEvent = new de.nameistaken.discore.command.slash.SlashCommandEvent(event, hook);
        Object[] methodArguments = new Object[arguments.length + 1];
        methodArguments[0] = commandEvent;
        for (int i = 0; i < arguments.length; i++) {
            SlashCommandArgument<?> argument = arguments[i];
            Result<?> result = argument.retrieveArgumentValue(event);
            if (result.isValid()) {
                // Argument konnte in Instanz der angegebenen Klasse umgewandelt werden
                methodArguments[i + 1] = result.getResult();
            } else {
                // Fehlermeldung des Ergebnisses an User schicken
                event.getHook().sendMessage(result.getErrorMessage()).queue();
                DCLogger.log(DCLogger.Type.INFO, this, author.getAsTag() + " failed to issue slashcommand [ConverterError] " + event.getCommandPath());
                return;
            }
        }

        // versuchen, Command auszuführen
        try {
            DCLogger.log(DCLogger.Type.INFO, this, author.getAsTag() + " issued slashcommand " + event.getCommandPath());
            commandMethod.invoke(commandInstance, methodArguments);
        } catch (IllegalAccessException e) {
            event.getHook().sendMessage(DisCore.getConfigValue("message.error.veryunexpected")).queue();
            DCLogger.log(DCLogger.Type.INFO, this, author.getAsTag() + " failed to issue slashcommand [IllegalAccessException] " + event.getCommandPath());
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            event.getHook().sendMessage(DisCore.formatConfigValue("message.error.unexpected", e.getCause().getClass().getName())).queue();
            DCLogger.log(DCLogger.Type.INFO, this, author.getAsTag() + " failed to issue slashcommand [InvocationTargetException] " + event.getCommandPath());
            e.getCause().printStackTrace();
        }
    }

    private boolean containsChannel(String[] channelIds, String channelId) {
        for (String id : channelIds) {
            if (channelId.equals(id)) {
                return true;
            }
        }
        return false;
    }
}
