package de.nameistaken.discore.command.slash;

import de.nameistaken.discore.DisCore;
import de.nameistaken.discore.util.Result;
import de.nameistaken.discore.util.DisCoreUtils;
import de.nameistaken.discore.converter.ConverterManager;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;

public class SlashCommandArgument<T> {
    private final String name;
    private final Class<T> clazz;
    private final boolean required;

    public SlashCommandArgument(String name, Class<T> clazz, boolean required) {
        this.name = name;
        this.clazz = clazz;
        this.required = required;
    }

    public String getName() {
        return name;
    }

    public Class<T> getClazz() {
        return clazz;
    }

    public Result<T> retrieveArgumentValue(SlashCommandEvent event) {
        Result<T> result = new Result<>(null, true);

        OptionMapping option = event.getOption(name);
        if (option == null) {
            if (required) {
                result = new Result<>();
                result.setErrorMessage(DisCore.formatConfigValue("message.error.slash.noargument", name)); //sollte von Discord Seiten verhindert werden, aber man weiß ja nie
            }
            return result;
        }

        Object value = null;
        if (clazz.isAssignableFrom(String.class)) {
            value = option.getAsString();
        } else if (clazz == int.class || clazz.isAssignableFrom(Integer.class) ||
                clazz == short.class || clazz.isAssignableFrom(Short.class) ||
                clazz == byte.class || clazz.isAssignableFrom(Byte.class) ||
                clazz == long.class || clazz.isAssignableFrom(Long.class)) {
            value = option.getAsLong();
        } else if (clazz == boolean.class || clazz.isAssignableFrom(Boolean.class)) {
            value = option.getAsBoolean();
        } else if (clazz.isAssignableFrom(User.class)) {
            value = option.getAsUser();
        } else if (clazz.isAssignableFrom(Member.class)) {
            value = option.getAsMember();
        } else if (clazz.isAssignableFrom(GuildChannel.class)) {
            value = option.getAsGuildChannel();
        } else if (clazz.isAssignableFrom(Role.class)) {
            value = option.getAsRole();
        } else if (clazz.isAssignableFrom(IMentionable.class)) {
            value = option.getAsMentionable();
        } else if (clazz.isAssignableFrom(ChoiceElement.class)) {
            for (ChoiceElement enumValue : DisCoreUtils.getEnumValues(clazz)) {
                if (enumValue.getName().equals(option.getAsString())) {
                    value = enumValue;
                }
            }
        }

        if (value == null) {
            if (ConverterManager.isConverterAvailable(clazz)) {
                result = ConverterManager.convertTo(option.getAsString(), clazz);
            } else if (required) {
                result = new Result<>();
                result.setErrorMessage(DisCore.formatConfigValue("message.error.slash.noconversion", option.getAsString(), clazz.getSimpleName()));
            }
        }else{
            result = new Result<>((T)value);
        }
        return result;
    }
}
