package de.nameistaken.discore.command.slash;

import java.lang.annotation.*;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(DescriptionContainer.class)
public @interface ArgumentDescription {
    /**
     * @return index des parameters, der durch diese Annotation beschrieben wird, wobei bei 0 angefangen wird und das SlashCommandEvent als Parameter mitgezählt wird (das erste relevante Argument hat also den index 1)
     */
    int parameterIndex();

    String description();

    String name() default ""; //wird defaultmäßig auf den Namen des Parameters gesetzt
}
