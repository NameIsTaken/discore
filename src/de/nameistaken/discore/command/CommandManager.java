package de.nameistaken.discore.command;

import de.nameistaken.discore.DisCore;
import de.nameistaken.discore.command.message.CommandContainer;
import de.nameistaken.discore.command.reaction.ReactionCommandContainer;
import de.nameistaken.discore.command.slash.SlashCommandContainer;
import de.nameistaken.discore.util.Emote;
import de.nameistaken.discore.messenger.Messenger;
import de.nameistaken.discore.events.EventListener;
import de.nameistaken.discore.util.DCLogger;
import de.nameistaken.discore.embeder.ErrorEmbeder;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.events.message.react.GenericMessageReactionEvent;
import net.dv8tion.jda.api.interactions.InteractionHook;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public final class CommandManager {
    private static CommandManager instance;

    public static CommandManager initInstance(String commandPrefix, Set<CommandContainer> commands, Set<SlashCommandContainer> slashCommands, Set<ReactionCommandContainer> reactionCommands, boolean forcePrefix, boolean deleteUnknown) {
        if (instance != null) {
            throw new IllegalStateException("There is already an instance of CommandManager");
        }
        instance = new CommandManager(commandPrefix, commands, slashCommands, reactionCommands, forcePrefix, deleteUnknown);
        return instance;
    }

    public static CommandManager getInstance() {
        return instance;
    }

    private final String commandPrefix;
    private final Map<String, CommandContainer> commands;
    private final Map<String, SlashCommandContainer> slashCommands;
    private final Map<Emote, ReactionCommandContainer> reactionCommands;

    /**
     * Falls true müssen commands im Privatchat mit commandprefix anfangen, sonst nicht
     */
    private final boolean forcePrefix;

    /**
     * Falls true werden unbekannte Commands in öffentlichen Channeln gelöscht, sonst nicht
     */
    private final boolean deleteUnknown;

    private CommandManager(String commandPrefix, Set<CommandContainer> commands, Set<SlashCommandContainer> slashCommands, Set<ReactionCommandContainer> reactionCommands, boolean forcePrefix, boolean deleteUnknown) {
        this.commandPrefix = commandPrefix;
        this.commands = new ConcurrentHashMap<>();
        this.slashCommands = new ConcurrentHashMap<>();
        this.reactionCommands = new ConcurrentHashMap<>();
        this.forcePrefix = forcePrefix;
        this.deleteUnknown = deleteUnknown;
        init(commands, slashCommands, reactionCommands);
    }

    private void init(Set<CommandContainer> commands, Set<SlashCommandContainer> slashCommands, Set<ReactionCommandContainer> reactionCommands) {
        for (CommandContainer command : commands) {
            this.commands.put(command.getName(), command);
            if (command.getAliasses().length != 0) {
                for (String alias : command.getAliasses()) {
                    this.commands.put(alias.toLowerCase(), command);
                }
            }
        }

        for (SlashCommandContainer slashCommand : slashCommands) {
            this.slashCommands.put(slashCommand.getKey(), slashCommand);
        }

        for (ReactionCommandContainer reactioncommand : reactionCommands) {
            for (Emote emote : reactioncommand.getEmotes()) {
                this.reactionCommands.put(emote, reactioncommand);
            }
        }
    }

    @EventListener
    public void onMessageReceived(MessageReceivedEvent event) {
        Message m = event.getMessage();
        if (m.isWebhookMessage() || m.getAuthor().isBot()) {
            return;
        }
        if (!m.getContentRaw().startsWith(commandPrefix) && !(!forcePrefix && event.isFromType(ChannelType.PRIVATE))) {
            return;
        }
        String[] arguments = m.getContentRaw().split(" +");
        String name = arguments[0];
        if (name.startsWith(commandPrefix)) {
            name = name.substring(commandPrefix.length());
        }
        CommandContainer command = commands.get(name);
        if (command == null) {
            if (deleteUnknown || event.isFromType(ChannelType.PRIVATE)) {
                Messenger.get(event.getAuthor()).sendMessage(new ErrorEmbeder().setDescriptionKey("message.error.nocommand"));
                event.getMessage().delete().queue();
            }
            DCLogger.log(DCLogger.Type.INFO, this, event.getAuthor().getAsTag() + " failed to issue command [UnknownCommand]: " + event.getMessage().getContentRaw());
            return;
        } else if (arguments.length > 1) {
            arguments = Arrays.copyOfRange(arguments, 1, arguments.length);
        } else {
            arguments = new String[0];
        }
        if (command.callCommand(event, arguments)) {
            if (event.isFromType(ChannelType.TEXT)) {
                event.getMessage().delete().queue();
            }
        }
    }

    @EventListener
    public void onSlashCommand(SlashCommandEvent event) {
        //damit discord ruhe gibt erstmal bestätigen, dass wir den command bekommen haben, alles andere kann danach behandelt werden
        InteractionHook hook = event.deferReply().complete();
        SlashCommandContainer command = slashCommands.get(event.getCommandPath());
        if (command == null) {
            event.getHook().editOriginal(DisCore.getConfigValue("message.error.nocommand")).queue();
            DCLogger.log(DCLogger.Type.INFO, this, event.getUser().getAsTag() + " failed to issue slashcommand [UnknownCommand]: " + event.getCommandPath());
        } else {
            command.callCommand(event, hook);
        }
    }

    @EventListener
    public void onReaction(GenericMessageReactionEvent event) {
        if (event.getUser() == null || event.getUser().isBot()) {
            return;
        }
        Emote emote = Emote.getReaction(event.getReaction().getReactionEmote().getName());
        if (emote == null) {
            return;
        }
        ReactionCommandContainer container = reactionCommands.get(emote);
        if (container == null) {
            return;
        }
        if (container.callCommand(event, emote) && !event.isFromType(ChannelType.PRIVATE)) {
            event.getReaction().removeReaction(event.getUser()).queue();
        }
    }

    public Collection<CommandContainer> getRegisteredCommands() {
        return commands.values();
    }

    public CommandContainer getCommandByName(String name) {
        return commands.get(name);
    }
}
