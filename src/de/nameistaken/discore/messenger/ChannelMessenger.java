package de.nameistaken.discore.messenger;

import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.MessageEmbed;

import java.util.function.Consumer;

public class ChannelMessenger extends Messenger {
    private final MessageChannel channel;

    public ChannelMessenger(MessageChannel channel) {
        super();
        this.channel = channel;
    }

    @Override
    public void sendMessage(String message, Consumer<Message> onSuccess, Consumer<? super Throwable> onError) {
        channel.sendMessage(message).queue(onSuccess, onError);
    }

    @Override
    public void sendMessage(MessageEmbed message, Consumer<Message> onSuccess, Consumer<? super Throwable> onError) {
        channel.sendMessage(message).queue(onSuccess, onError);
    }

    @Override
    public void sendMessage(String preMessage, MessageEmbed embed, Consumer<Message> onSuccess, Consumer<? super Throwable> onError) {
        channel.sendMessage(preMessage).embed(embed).queue(onSuccess, onError);
    }
}
