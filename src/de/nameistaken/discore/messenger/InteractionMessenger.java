package de.nameistaken.discore.messenger;

import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.interactions.InteractionHook;

import java.util.function.Consumer;

public class InteractionMessenger extends Messenger {
    private final InteractionHook hook;
    private boolean editedOriginal;

    public InteractionMessenger(InteractionHook hook) {
        this.hook = hook;
        editedOriginal = false;
    }

    @Override
    public void sendMessage(String message, Consumer<Message> onSuccess, Consumer<? super Throwable> onError) {
        if(editedOriginal){
            hook.sendMessage(message).queue(onSuccess, onError);
        }else{
            editedOriginal = true;
            hook.editOriginal(message).queue(onSuccess, onError);
        }
    }

    @Override
    public void sendMessage(MessageEmbed message, Consumer<Message> onSuccess, Consumer<? super Throwable> onError) {
        if(editedOriginal){
            hook.sendMessageEmbeds(message).queue(onSuccess, onError);
        }else{
            editedOriginal = true;
            hook.editOriginal(new MessageBuilder(message).build()).queue(onSuccess, onError);
        }
    }

    @Override
    public void sendMessage(String preMessage, MessageEmbed embed, Consumer<Message> onSuccess, Consumer<? super Throwable> onError) {
        if(editedOriginal){
            hook.sendMessage(preMessage).addEmbeds(embed).queue(onSuccess, onError);
        }else {
            editedOriginal = true;
            hook.editOriginal(preMessage).setEmbeds(embed).queue(onSuccess, onError);
        }
    }
}
