package de.nameistaken.discore.messenger;

import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.User;

import java.util.function.Consumer;

public class UserMessenger extends Messenger {
    private final User user;

    public UserMessenger(User user) {
        super();
        this.user = user;
    }

    @Override
    public void sendMessage(String message, Consumer<Message> onSuccess, Consumer<? super Throwable> onError) {
        user.openPrivateChannel().queue((channel) -> channel.sendMessage(message).queue(onSuccess, onError));
    }

    @Override
    public void sendMessage(MessageEmbed message, Consumer<Message> onSuccess, Consumer<? super Throwable> onError) {
        user.openPrivateChannel().queue((channel) -> channel.sendMessage(message).queue(onSuccess, onError));
    }

    @Override
    public void sendMessage(String preMessage, MessageEmbed embed, Consumer<Message> onSuccess, Consumer<? super Throwable> onError) {
        user.openPrivateChannel().queue((channel) -> channel.sendMessage(preMessage).embed(embed).queue(onSuccess, onError));
    }
}
