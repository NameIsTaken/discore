package de.nameistaken.discore.messenger;

import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;

import java.util.function.Consumer;

public class ReplyMessenger extends Messenger {
    private final Message message;

    public ReplyMessenger(Message message) {
        super();
        this.message = message;
    }

    @Override
    public void sendMessage(String message, Consumer<Message> onSuccess, Consumer<? super Throwable> onError) {
        this.message.reply(message).queue(onSuccess, onError);
    }

    @Override
    public void sendMessage(MessageEmbed message, Consumer<Message> onSuccess, Consumer<? super Throwable> onError) {
        this.message.reply(message).queue(onSuccess, onError);
    }

    @Override
    public void sendMessage(String preMessage, MessageEmbed embed, Consumer<Message> onSuccess, Consumer<? super Throwable> onError) {
        this.message.reply(preMessage).embed(embed).queue(onSuccess, onError);
    }
}
