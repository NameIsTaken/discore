package de.nameistaken.discore.messenger;

import de.nameistaken.discore.embeder.MessageEmbeder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.interactions.InteractionHook;

import java.util.function.Consumer;

/**
 * Klasse, die für Textchannel und Privatechannel (Direktnachricht an User) gleichermaßen genutzt werden kann um Nachrichten dorthin zu senden
 */
public abstract class Messenger {
    public static Messenger get(User user) {
        return new UserMessenger(user);
    }

    public static Messenger get(MessageChannel channel) {
        return new ChannelMessenger(channel);
    }

    public static Messenger get(Message message) {
        return new ReplyMessenger(message);
    }

    public static Messenger get(InteractionHook hook) {
        return new InteractionMessenger(hook);
    }

    public Messenger() {
    }

    /* ========== String messages ========== */

    public void sendMessage(String message) {
        sendMessage(message, null, null);
    }

    public void sendMessageAndThen(String message, Consumer<Message> onSuccess) {
        sendMessage(message, onSuccess, null);
    }

    public void sendMessageOrElse(String message, Consumer<? super Throwable> onError) {
        sendMessage(message, null, onError);
    }

    public abstract void sendMessage(String message, Consumer<Message> onSuccess, Consumer<? super Throwable> onError);

    /* ========== MessageEmbeder messages ========== */

    public void sendMessage(MessageEmbeder message) {
        sendMessage(message, null, null);
    }

    public void sendMessageAndThen(MessageEmbeder message, Consumer<Message> onSuccess) {
        sendMessage(message, onSuccess, null);
    }

    public void sendMessageOrElse(MessageEmbeder message, Consumer<? super Throwable> onError) {
        sendMessage(message, null, onError);
    }

    public void sendMessage(MessageEmbeder message, Consumer<Message> onSuccess, Consumer<? super Throwable> onError) {
        sendMessage(message.build(), onSuccess, onError);
    }

    /* ========== MessageEmbed messages ========== */

    public void sendMessage(MessageEmbed message) {
        sendMessage(message, null, null);
    }

    public void sendMessageAndThen(MessageEmbed message, Consumer<Message> onSuccess) {
        sendMessage(message, onSuccess, null);
    }

    public void sendMessageOrElse(MessageEmbed message, Consumer<? super Throwable> onError) {
        sendMessage(message, null, onError);
    }

    public abstract void sendMessage(MessageEmbed message, Consumer<Message> onSuccess, Consumer<? super Throwable> onError);

    /* ========== String & Embed ========== */

    public void sendMessage(String preMessage, MessageEmbed embed) {
        sendMessage(preMessage, embed, null, null);
    }

    public void sendMessageAndThen(String preMessage, MessageEmbed embed, Consumer<Message> onSuccess) {
        sendMessage(preMessage, embed, onSuccess, null);
    }

    public void sendMessageOrElse(String preMessage, MessageEmbed embed, Consumer<? super Throwable> onError) {
        sendMessage(preMessage, embed, null, onError);
    }

    public abstract void sendMessage(String preMessage, MessageEmbed embed, Consumer<Message> onSuccess, Consumer<? super Throwable> onError);
}
