package de.nameistaken.discore.events;

import net.dv8tion.jda.api.events.GenericEvent;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class EventContainer<E extends GenericEvent> {
    private final Object invokeObject;
    private final Method method;
    private final Class<E> eventClass;
    private long lastEventResponseNumber;

    public EventContainer(Object invokeObject, Method method, Class<E> eventClass) {
        this.invokeObject = invokeObject;
        this.method = method;
        this.eventClass = eventClass;
        this.lastEventResponseNumber = -1;
    }

    void onEvent(GenericEvent event) {
        if (!isValidEvent(event)) {
            throw new IllegalStateException(String.format("Tried to call event on wrong event method. Method class: %s, actual event class: %s", eventClass.getName(), event.getClass().getName()));
        }
        lastEventResponseNumber = event.getResponseNumber();
        try {
            method.invoke(invokeObject, event);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public boolean isValidEvent(GenericEvent event) {
        return event.getResponseNumber() != lastEventResponseNumber && eventClass.isAssignableFrom(event.getClass());
    }
}
