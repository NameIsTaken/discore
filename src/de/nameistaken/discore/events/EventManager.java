package de.nameistaken.discore.events;

import de.nameistaken.discore.util.DCLogger;
import net.dv8tion.jda.api.events.GenericEvent;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

public final class EventManager implements net.dv8tion.jda.api.hooks.EventListener {
    private static EventManager instance;

    public static EventManager initInstance(Set<EventContainer<?>> eventContainers) {
        if (instance != null) {
            throw new IllegalStateException("There is already an instance of EventManager");
        }
        instance = new EventManager(eventContainers);
        return instance;
    }

    public static EventManager getInstance() {
        return instance;
    }

    private final Set<EventContainer<?>> eventContainers;
    private final Queue<GenericEvent> events;
    private boolean running = false;

    private EventManager(Set<EventContainer<?>> eventContainers) {
        this.eventContainers = eventContainers;
        this.events = new LinkedList<>();
    }

    @Override
    public synchronized void onEvent(@NotNull GenericEvent event) {
        if (!running) {
            events.add(event);
        } else {
            fireEvent(event);
        }
    }

    public synchronized void start() {
        if (running) {
            throw new IllegalStateException("Already started");
        }
        running = true;
        fireQueuedEvents();
    }

    public void stop() {
        if (!running) {
            throw new IllegalStateException("Is not started");
        }
        running = false;
    }

    private void fireQueuedEvents() {
        while (!events.isEmpty()) {
            fireEvent(events.poll());
        }
    }

    private void fireEvent(GenericEvent event) {
        if (event == null) {
            DCLogger.log(DCLogger.Type.WARN, EventManager.class, "Stopped null event from being fired");
            return;
        }
        eventContainers.stream()
                .filter(container -> container.isValidEvent(event))
                .forEach(container -> container.onEvent(event));
    }
}
