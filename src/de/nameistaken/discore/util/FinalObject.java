package de.nameistaken.discore.util;

/**
 * Klasse für verdammte Lambdas, die mich nicht das machen lassen wollen, was ich will. Arschloch
 *
 * @author NameIsTaken
 * @version 07.03.2020
 */
public class FinalObject<E> {
    private E object;

    public FinalObject() {
    }

    public FinalObject(E object) {
        this.object = object;
    }

    public E get() {
        return object;
    }

    public void set(E object) {
        this.object = object;
    }
}
