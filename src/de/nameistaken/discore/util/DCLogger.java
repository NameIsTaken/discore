package de.nameistaken.discore.util;

import java.io.PrintStream;

/**
 * Logger zum loggen von Nachrichten
 *
 * @author NameIsTaken
 * @version 25.10.2019
 */
public final class DCLogger {
    private static boolean debug = false;
    private static final String lnString = "=========================";

    private static void log(Type loggingType, String className, String logMessage, boolean exactTime) {
        if (debug || loggingType != Type.DEBUG) {
            PrintStream ps = System.out;
            if (loggingType == Type.WARN || loggingType == Type.ERROR) {
                ps = System.err;
            }
            String timestamp = exactTime ? DisCoreUtils.getExactDebugTimestampString() : DisCoreUtils.getDebugTimestampString();
            ps.printf("[%s] [%s] [%s] %s\n", timestamp, loggingType, className, logMessage);
        }
    }

    public static void log(Type loggingType, Class<?> clazz, String logMessage, boolean exactTime) {
        log(loggingType, clazz.getSimpleName(), logMessage, exactTime);
    }

    public static void log(Type loggingType, Class<?> clazz, String logMessage) {
        log(loggingType, clazz.getSimpleName(), logMessage, false);
    }

    public static void log(Type loggingType, Object o, String logMessage, boolean exactTime) {
        String className = o instanceof String ? (String) o : o.getClass().getSimpleName();
        log(loggingType, className, logMessage, exactTime);
    }

    public static void log(Type loggingType, Object o, String logMessage) {
        log(loggingType, o, logMessage, false);
    }

    public static void logWithDivider(Type loggingType, Class<?> clazz, String logMessage, boolean exactTime) {
        log(loggingType, clazz.getSimpleName(), lnString, exactTime);
        log(loggingType, clazz.getSimpleName(), logMessage, exactTime);
        log(loggingType, clazz.getSimpleName(), lnString, exactTime);
    }

    public static void logWithDivider(Type loggingType, Class<?> clazz, String logMessage) {
        log(loggingType, clazz.getSimpleName(), lnString, false);
        log(loggingType, clazz.getSimpleName(), logMessage, false);
        log(loggingType, clazz.getSimpleName(), lnString, false);
    }

    public static void logWithDivider(Type loggingType, Object o, String logMessage, boolean exactTime) {
        String className = o instanceof String ? (String) o : o.getClass().getSimpleName();
        log(loggingType, className, lnString, exactTime);
        log(loggingType, className, logMessage, exactTime);
        log(loggingType, className, lnString, exactTime);
    }

    public static void logWithDivider(Type loggingType, Object o, String logMessage) {
        log(loggingType, o, lnString, false);
        log(loggingType, o, logMessage, false);
        log(loggingType, o, lnString, false);
    }

    public static void setDebug(boolean debug) {
        DCLogger.debug = debug;
    }

    public enum Type {
        WARN, ERROR, DEBUG, INFO
    }
}
