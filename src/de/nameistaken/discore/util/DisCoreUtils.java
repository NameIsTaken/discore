package de.nameistaken.discore.util;

import de.nameistaken.discore.command.slash.ChoiceElement;
import de.nameistaken.discore.exceptions.DisCoreException;
import net.dv8tion.jda.api.entities.IMentionable;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class DisCoreUtils {
    private static final SimpleDateFormat debugFormat;
    private static final SimpleDateFormat exactDebugFormat;

    static {
        debugFormat = new SimpleDateFormat("HH:mm:ss");
        exactDebugFormat = new SimpleDateFormat("HH:mm:ss.SSS");
    }

    public static String getDebugTimestampString() {
        Calendar currentDate = Calendar.getInstance();
        return debugFormat.format(currentDate.getTime());
    }

    public static String getExactDebugTimestampString() {
        Calendar currentDate = Calendar.getInstance();
        return exactDebugFormat.format(currentDate.getTime());
    }

    public static String formatTime(long time) {
        long days = TimeUnit.MILLISECONDS.toDays(time);
        time -= TimeUnit.DAYS.toMillis(days);
        long hours = TimeUnit.MILLISECONDS.toHours(time);
        time -= TimeUnit.HOURS.toMillis(hours);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(time);
        time -= TimeUnit.MINUTES.toMillis(minutes);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(time);
        StringBuilder sb = new StringBuilder();
        if (days > 0) {
            sb.append(days).append("d ").append(days != 1 ? "e " : " ");
        }
        if (hours > 0) {
            sb.append(hours).append("h ").append(hours != 1 ? "n " : " ");
        }
        if (minutes > 0) {
            sb.append(minutes).append("m ").append(minutes != 1 ? "n " : " ");
        }
        if (seconds > 0) {
            sb.append(seconds).append("s ").append(seconds != 1 ? "n " : " ");
        }
        return sb.toString().trim();
    }

    public static String listToString(List<IMentionable> list) {
        StringBuilder sb = new StringBuilder();
        for (IMentionable item : list) {
            sb.append(item.getAsMention()).append(" (").append(item.getId()).append(")\n");
        }
        return sb.toString();
    }

    /**
     * @param enumClass Class Objekt eines Enums, das {@link ChoiceElement} implementiert
     * @return Array an ChoiceElement Objekten die die Enum Werte repräsentieren
     */
    public static ChoiceElement[] getEnumValues(Class<?> enumClass) {
        try {
            Field valuesField = enumClass.getDeclaredField("$VALUES");
            valuesField.setAccessible(true);
            return (ChoiceElement[]) valuesField.get(null);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new DisCoreException("Unable extract enum values", e);
        }
    }
}
