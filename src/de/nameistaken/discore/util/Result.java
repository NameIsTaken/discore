package de.nameistaken.discore.util;

import de.nameistaken.discore.embeder.MessageEmbeder;

/**
 * Klasse für Results. Result kann entweder valid sein (Objekt != null) oder invalid (== null)
 * In zweitem Fall kann eine Fehlermeldung angegeben werden
 *
 * @author NameIsTaken
 * @version 24.10.2019
 */
public class Result<E> {
    private final E result;
    private String errorMessage;
    private final boolean valid;

    public Result(E result, boolean valid) {
        this.result = result;
        this.valid = valid;
    }

    public Result(E result) {
        this(result, result != null);
    }

    public Result() {
        this(null);
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean isValid() {
        return valid;
    }

    public E getResult() {
        return result;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public MessageEmbeder errorEmbedBuilder() {
        return new MessageEmbeder()
                .errorPreset()
                .append(errorMessage);
    }

    @Override
    public String toString() {
        String s = "Result: " + (isValid() ? String.valueOf(result) : ("[Error] " + errorMessage));
        return s;
    }
}
